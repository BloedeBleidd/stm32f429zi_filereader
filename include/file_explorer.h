/*
 * file_explorer.h
 *
 *  Created on: Sep 18, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef FILE_EXPLORER_H_
#define FILE_EXPLORER_H_

#include "stm32f4xx.h"
#include <vector>
#include "RTOS_includes.h"
#include "FAT_includes.h"
#include "graphic.h"
#include "file.h"
#include "colors.h"

class FileExplorer
{
public:
	enum SortType{	NameAscending, NameDescending, NameAscendingFoldersFirst, NameDescendingFoldersFirst,
					DateAscending, DateDescending, DateAscendingFoldersFirst, DateDescendingFoldersFirst };

private:
	typedef std::vector<FileBase *> List;
	struct Coordinate{ int32_t x,y; };
	struct Delay{ TickType_t state, scroll; };
	struct Size{ int32_t width, height; };
	struct SegmentState{ int32_t highlighted; int32_t selected; };
	struct WindowData{ Coordinate position; Size size; Color color; };
	struct SegmentData{ Size size; Font::Data font; Color colorFont; Color colorBorder; Color colorBackground; Color colorHighlight; SegmentState state; };

	static const int32_t ERROR = -1;

	List list;
	WindowData window;
	SegmentData segment;
	Coordinate lastTouch;
	TickType_t lastTime;
	int32_t offset;
	Delay delay;

	void drawSegment( Graphic<Color,int32_t> &lcd, int32_t y, const char * str, bool highlighted ) const;
	bool isOverflowed() const;
	void unallocateWhole();
	int32_t whichSegment( int32_t x, int32_t y ) const;

public:
	FileExplorer( int32_t x, int32_t y, int32_t width, int32_t height, Color backgroundColor, Color segmentColor, Color borderColor, Color highlightColor, Color fontColor, Font::Data font, TickType_t delayState, TickType_t delayScroll );
	~FileExplorer();

	void sort( SortType );
	void append( FileBase * );
	void clear();
	void draw( Graphic<Color,int32_t> & ) const;
	bool refresh( int32_t x, int32_t y );
	bool getSelected( FileBase ** ) const;
	bool getHighlighted( FileBase ** ) const;
	void resetSelectAndHighlight()	{ segment.state.selected = ERROR; segment.state.highlighted = ERROR; }
	void reset()					{ resetSelectAndHighlight(); offset = 0; }
	FRESULT scanDirectory( const char *path );
	static bool isFormat( const File &file, const char **formatTable );
	bool isFormat( const File &file, const char *format );
};

#endif /* FILE_EXPLORER_H_ */
