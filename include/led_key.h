/*
 * led_key.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef LED_KEY_H_
#define LED_KEY_H_


#define KEY_GPIO		GPIOA
#define KEY_PIN			0

#define LED_GREEN_GPIO	GPIOG
#define LED_GREEN_PIN	13

#define LED_RED_GPIO	GPIOG
#define LED_RED_PIN		14


#endif /* LED_KEY_H_ */
