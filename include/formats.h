/*
 * formats.h
 *
 *  Created on: Sep 20, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef FORMATS_H_
#define FORMATS_H_


const char *formatText[] = { "txt", "TXT", "c", "C", "cpp", "CPP", "h", "H", "cfg", "CFG", "m", "M", "doc", "DOC", "" };
const char *formatGraphic[] = { "bmp", "BMP", "jpg", "JPG", "jpeg", "JPEG", "gif", "GIF", "png", "PNG", "" };
const char *formatAudio[] = { "wav", "mp3", "wma", "" };
const char *formatVideo[] = { "mp4", "3gp", "mkv", "" };


#endif /* FORMATS_H_ */
