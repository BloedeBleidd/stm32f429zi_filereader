/*
 * task_include.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef TASK_INCLUDE_H_
#define TASK_INCLUDE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "RTOS_includes.h"

#define priorityStatusLed		(priorityLOWEST)
#define stackStatusLed			(configSTACK_DEPTH_TYPE)(500)
void taskStatusLed( void * p );

#define priorityTouchPanel		(priorityHIGHER)
#define stackTouchPanel			(configSTACK_DEPTH_TYPE)(500)
void taskTouchPanel( void * p );

#define priorityMain			(priorityNORMAL)
#define stackMain				(configSTACK_DEPTH_TYPE)(30000)
void taskMain( void * p );

#define priorityCpuUtility		(priorityHIGHEST)
#define stackCpuUtility			(configSTACK_DEPTH_TYPE)(500)
void taskCpuUtility( void * p );

#ifdef __cplusplus
}
#endif

#endif /* TASK_INCLUDE_H_ */
