
#include <sys/types.h>
#include <errno.h>

caddr_t
_sbrk(int incr);

// The definitions used here should be kept in sync with the
// stack definitions in the linker script.

caddr_t _sbrk( int incr )
{
	static char *sdram_start = (char*) 0xD0000000;
	static char *sdram_end = (char*) 0xD0800000;
	static char *heap_end;

	if (heap_end == 0)
		heap_end = sdram_start;

	char *prev_heap_end = heap_end;

	if( heap_end + incr > sdram_end )
		return (caddr_t) -1;

	heap_end += incr;

	return (caddr_t) prev_heap_end;
}
