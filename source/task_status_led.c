/*
 * status_led.c
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "RTOS_includes.h"
#include "task_include.h"
#include "gpio.h"
#include "led_key.h"

void taskStatusLed( void * p )
{
	(void)(p);
	const TickType_t DELAY = 250;
	TickType_t lastTime = xTaskGetTickCount();
	gpioPinConfiguration( LED_GREEN_GPIO, LED_GREEN_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );

	for(;;)
	{
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS( DELAY ) );
		gpioBitToggle( LED_GREEN_GPIO, LED_GREEN_PIN );
	}
}
