/*
 * reader_text.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "FAT_includes.h"
#include "task_include.h"
#include "gpio.h"
#include "led_key.h"
#include "ili9341_ltdc.h"
#include "stmpe811.h"
#include "colors.h"
#include "file.h"
#include "button_text_border_flat.h"

extern QueueHandle_t  queueTouchPanel;


int32_t countLines( const char *s )
{
	int32_t cnt = 1;
	int32_t len = strlen(s);

	for( int32_t i=0; i<len; i++ )
		if( s[i] == '\n' )
			cnt++;

	return cnt;
}

int32_t findLongest( const char *s )
{
	int32_t cnt = 0;
	int32_t len = strlen(s);
	int32_t tmp = 0;

	for( int32_t i=0; i<len; i++ )
	{
		tmp++;

		if( s[i] == '\n' )
		{
			if( tmp > cnt )
				cnt = tmp;

			tmp = 0;
		}
	}

	if( tmp > cnt )
		cnt = tmp;

	return cnt;
}

bool readerText( Ili9341_LTDC &lcd, const Directory &directory, const File &file )
{
	const TickType_t DELAY = 200;
	const UINT MAX_BUFFER_LENGHT = 100000;
	const char *path = (directory+file.getName()).get();

	FIL filePointer;

	if( FR_OK != f_open( &filePointer, path, FA_READ ) )
		return false;

	UINT amountOut;
	UINT amountIn = f_size( &filePointer );

	if( amountIn > MAX_BUFFER_LENGHT )
		amountIn = MAX_BUFFER_LENGHT;

	char *buffer = new char[ amountIn + 1 ];

	if( FR_OK == f_read( &filePointer, buffer, amountIn, &amountOut ) )
	{
		if( FR_OK != f_close( &filePointer ) )
		{
			delete [] buffer;
			return false;
		}

		buffer[amountIn] = '\0';
		ButtonTextBorderFlat<Color,int32_t> exitButton( lcd.getWidth()-51, lcd.getHeight()-31, 50, 30, GREEN, RED, BLACK, "EXIT", Font::Font11x18 );
		Font::Data fontTmp = lcd.getFont();
		lcd.setFont( Font::Font7x10 );
		lcd.fill( WHITE );
		lcd.drawString( 0, 0, buffer, BLACK );
		exitButton.draw( lcd );
		lcd.refreshCurrentLayer();

		int32_t maxOffsetX = findLongest( buffer )*lcd.getFontWidth();
		int32_t maxOffsetY = countLines( buffer )*lcd.getFontHeight();

		if( maxOffsetX < lcd.getWidth() )
			maxOffsetX = 0;
		else
			maxOffsetX -= lcd.getWidth();

		if( maxOffsetY < lcd.getHeight() )
			maxOffsetY = 0;
		else
			maxOffsetY -= lcd.getHeight();

		Stmpe811<int32_t>::Coordinates lastTouch = { 0, 0, 0 };
		int32_t offsetX = 0, offsetY = 0;
		TickType_t lastTime = xTaskGetTickCount();

		for(;;)
		{
			Stmpe811<int32_t>::Coordinates panel;

			if( pdTRUE == xQueueReceive( queueTouchPanel, (void*) &panel, 2 ) )
			{
				TickType_t time = xTaskGetTickCount();

				if( time < lastTime + DELAY )
				{
					offsetX += lastTouch.x - panel.x;
					offsetY += lastTouch.y - panel.y;

					if( offsetX < 0 )
						offsetX = 0;
					else if( offsetX > maxOffsetX )
						offsetX = maxOffsetX;

					if( offsetY < 0 )
						offsetY = 0;
					else if( offsetY > maxOffsetY )
						offsetY = maxOffsetY;

					lcd.fill( WHITE );
					lcd.drawString( -offsetX, -offsetY, buffer, BLACK );
					exitButton.draw( lcd );
					lcd.refreshCurrentLayer();
				}

				if( exitButton.isPressed( panel.x, panel.y ) )
					break;

				lastTouch = panel;
				lastTime = time;
				xQueueReset( queueTouchPanel );
			}
		}

		lcd.setFont( fontTmp );
	}
	else
	{
		delete [] buffer;
		return false;
	}

	delete [] buffer;
	xQueueReset( queueTouchPanel );
	return true;
}
