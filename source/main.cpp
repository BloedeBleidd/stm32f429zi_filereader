/*
 * main.cpp
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "RTOS_includes.h"
#include "task_include.h"

int main(void)
{
	xTaskCreate( taskStatusLed, "status led", stackStatusLed, NULL, priorityStatusLed, NULL );
	xTaskCreate( taskTouchPanel, "Touch Panel", stackTouchPanel, NULL, priorityTouchPanel, NULL );
	xTaskCreate( taskMain, "main task", stackMain, NULL, priorityMain, NULL );
	xTaskCreate( taskCpuUtility, "cpu utility", stackCpuUtility, NULL, priorityCpuUtility, NULL );
	vTaskStartScheduler();

	return 0;
}


