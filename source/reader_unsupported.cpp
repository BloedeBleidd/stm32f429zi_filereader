/*
 * reader_unsupported.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "FAT_includes.h"
#include "task_include.h"
#include "gpio.h"
#include "led_key.h"
#include "ili9341_ltdc.h"
#include "stmpe811.h"
#include "colors.h"
#include "file.h"
#include "button_text_border_transparent.h"

extern QueueHandle_t  queueTouchPanel;


bool readerUnsupported( Ili9341_LTDC &lcd, const Directory &directory, const File &file )
{
	const char ERROR[] = "UNSUPPORTED FORMAT";

	ButtonTextBorderTransparent<Color,int32_t> exitButton( (lcd.getWidth()-50)/2, (lcd.getHeight()-30)/2+30, 50, 30, YELLOW, GREEN, "EXIT", Font::Font11x18 );
	lcd.fill( BLACK );
	Font::Data tmpFont = lcd.getFont();
	lcd.setFont( Font::Font16x26 );
	lcd.drawString( (lcd.getWidth()-sizeof(ERROR)*lcd.getFontWidth())/2, (lcd.getHeight()-lcd.getFontHeight())/2, ERROR, RED );
	exitButton.draw( lcd );
	lcd.refreshCurrentLayer();
	lcd.setFont( tmpFont );
	Stmpe811<int32_t>::Coordinates panel;

	for(;;)
	{
		if( pdTRUE == xQueueReceive( queueTouchPanel, (void*) &panel, 10 ) )
		{
			if( exitButton.isPressed( panel.x, panel.y ) )
				break;
		}
	}

	return true;
}
