/*
 * task_TouchPanel.cpp
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "RTOS_includes.h"
#include "task_include.h"
#include "stmpe811.h"
#include "stm32f4xx.h"
#include "Trace.h"

const uint16_t queueTouchPanel_SIZE = 100;
QueueHandle_t  queueTouchPanel;

typedef Stmpe811<int32_t> TP;

void taskTouchPanel( void * p )
{
	(void)(p);
	const TickType_t DELAY = 1000/100; //1000/fps
	const TP::I2CInitStruct i2c{ I2C3, 4, GPIOA, 8, GPIOC, 9 };

	RCC->APB1ENR |= RCC_APB1ENR_I2C3EN;
	TP tp( 320, 240, 30, 305, 15, 225, Orientation::Landscape_1, i2c );
	queueTouchPanel = xQueueCreate( queueTouchPanel_SIZE, sizeof( TP::Coordinates ) );
	TickType_t lastTime = xTaskGetTickCount();

	for(;;)
	{
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS( DELAY ) );

		if( true == tp.update() )
		{
			TP::Coordinates coord;
			coord.x  = tp.getX();
			coord.y = tp.getY();
			coord.z = tp.getZ();
			xQueueSend( queueTouchPanel, ( void * ) &coord, ( TickType_t ) 0 );
			trace_printf( "Touchpad {x,y,z}={%d,%d,%d}\r\n", coord.x, coord.y, coord.z );
		}
	}
}
