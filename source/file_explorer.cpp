/*
 * file_explorer.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "file_explorer.h"
#include <algorithm>
#include <cstring>
#include "Trace.h"

FileExplorer::FileExplorer( int32_t x, int32_t y, int32_t width, int32_t height, Color backgroundColor, Color segmentColor, Color borderColor, Color highlightColor, Color fontColor, Font::Data font, TickType_t delayState, TickType_t delayScroll )
{
	trace_puts( "FileExplorer constructor" );

	window.position.x = x;
	window.position.y = y;
	window.size.width = width;
	window.size.height = height;
	window.color = backgroundColor;
	segment.size.width = width;
	segment.size.height = font.height+2;
	segment.font = font;
	segment.colorFont = fontColor;
	segment.colorBorder = borderColor;
	segment.colorBackground = segmentColor;
	segment.colorHighlight = highlightColor;
	segment.state.highlighted = ERROR;
	segment.state.selected = ERROR;
	lastTouch.x = 0;
	lastTouch.y = 0;
	lastTime = xTaskGetTickCount();
	offset = 0;
	delay.state = delayState;
	delay.scroll = delayScroll;
}

FileExplorer::~FileExplorer()
{
	trace_puts( "FileExplorer destructor" );
	unallocateWhole();
}

void FileExplorer::drawSegment( Graphic<Color,int32_t> &lcd, int32_t y, const char * str, bool highlighted ) const
{
	trace_puts( "FileExplorer drawSegment" );
	lcd.drawRectangleAndFill( window.position.x, y, window.position.x + window.size.width, y + segment.size.height, highlighted?segment.colorHighlight:segment.colorBackground );
	lcd.drawRectangle( window.position.x, y, window.position.x + window.size.width, y + segment.size.height, segment.colorBorder );
	lcd.drawString( window.position.x + 1, 1 + y + ( segment.size.height - segment.font.height ) / 2, str, segment.colorFont );
}

bool FileExplorer::isOverflowed() const
{
	trace_puts( "FileExplorer isOverflowed" );
	if( (int32_t)list.size() * segment.size.height > window.size.height )
		return true;
	else
		return false;
}

void FileExplorer::unallocateWhole()
{
	trace_puts( "FileExplorer unallocateWhole" );
	for( int32_t i=list.size()-1; i>=1; i-- )
		delete list[i];
}

int32_t FileExplorer::whichSegment( int32_t x, int32_t y ) const
{
	trace_puts( "FileExplorer whichSegment" );
	if( y > window.position.y + window.size.height || y < window.position.y || x < window.position.x || x > window.position.x + window.size.width )
		return ERROR;

	int32_t tmp = ( y + offset ) / segment.size.height;

	if( (int32_t)list.size() > tmp )
		return tmp;
	else
		return ERROR;
}

static bool sortNameAscendingFoldersFirst( const FileBase *a , const FileBase *b )
{
	if( FileBase::Type::File == a->getType() && FileBase::Type::Folder == b->getType() )
		return false;
	else if( FileBase::Type::Folder == a->getType() && FileBase::Type::File == b->getType() )
		return true;
	else
		if( 0 > strcmp( a->getName(), b->getName() ) )
			return true;
		else
			return false;
}

static bool sortNameDescendingFoldersFirst( const FileBase *a , const FileBase *b )
{
	if( FileBase::Type::File == a->getType() && FileBase::Type::Folder == b->getType() )
		return false;
	else if( FileBase::Type::Folder == a->getType() && FileBase::Type::File == b->getType() )
		return true;
	else
		if( 0 > strcmp( a->getName(), b->getName() ) )
			return false;
		else
			return true;
}

static bool sortDateAscendingFoldersFirst( const FileBase *a , const FileBase *b )
{
	if( FileBase::Type::File == a->getType() && FileBase::Type::Folder == b->getType() )
		return false;
	else if( FileBase::Type::Folder == a->getType() && FileBase::Type::File == b->getType() )
		return true;
	else
		if( a->getTime() > b->getTime() )
			return true;
		else
			return false;
}

static bool sortDateDescendingFoldersFirst( const FileBase *a , const FileBase *b )
{
	if( FileBase::Type::File == a->getType() && FileBase::Type::Folder == b->getType() )
		return false;
	else if( FileBase::Type::Folder == a->getType() && FileBase::Type::File == b->getType() )
		return true;
	else
		if( a->getTime() < b->getTime() )
			return false;
		else
			return true;
}

void FileExplorer::sort( SortType sort )
{
	switch(sort)
	{
	case NameAscending:
		std::sort( list.begin(), list.end(), []( const FileBase *a , const FileBase *b )->bool { return strcmp( a->getName(), b->getName() )<0?true:false; } );
		break;
	case NameDescending:
		std::sort( list.begin(), list.end(), []( const FileBase *a , const FileBase *b )->bool { return strcmp( a->getName(), b->getName() )>0?true:false; } );
		break;
	case NameAscendingFoldersFirst:
		std::sort( list.begin(), list.end(), sortNameAscendingFoldersFirst );
		break;
	case NameDescendingFoldersFirst:
		std::sort( list.begin(), list.end(), sortNameDescendingFoldersFirst );
		break;
	case DateAscending:
		std::sort( list.begin(), list.end(), []( const FileBase *a , const FileBase *b )->bool { return (a->getTime() > b->getTime())?true:false; } );
		break;
	case DateDescending:
		std::sort( list.begin(), list.end(), []( const FileBase *a , const FileBase *b )->bool { return (a->getTime() < b->getTime())>0?true:false; } );
		break;
	case DateAscendingFoldersFirst:
		std::sort( list.begin(), list.end(), sortDateAscendingFoldersFirst );
		break;
	case DateDescendingFoldersFirst:
		std::sort( list.begin(), list.end(), sortDateDescendingFoldersFirst );
		break;
	default:
		break;
	}
}

void FileExplorer::append( FileBase *unit )
{
	trace_puts( "FileExplorer append" );
	list.push_back( unit );
}

void FileExplorer::clear()
{
	trace_puts( "FileExplorer clear" );
	unallocateWhole();
	list.clear();
}

void FileExplorer::draw( Graphic<Color,int32_t> &lcd ) const
{
	trace_puts( "FileExplorer draw" );
	Font::Data fontTmp = lcd.getFont();
	lcd.setFont( segment.font );
	lcd.drawRectangleAndFill( window.position.x, window.position.y, window.position.x+window.size.width, window.position.y+window.size.height, window.color );

	for( int32_t i=0; i<(int32_t)list.size(); i++ )
	{
		int32_t tmp = window.position.y + i*segment.size.height - offset;

		if( tmp >= -segment.size.height && tmp < window.position.y+window.size.height+segment.size.height )
			drawSegment( lcd, tmp, list[i]->getName(), ( i == segment.state.highlighted )?true:false );
	}

	lcd.setFont( fontTmp );
}

bool FileExplorer::refresh( int32_t x, int32_t y )
{
	trace_puts( "FileExplorer refresh" );
	if( x < window.position.x || x > ( window.position.x + window.size.width ) || y < window.position.y || y > ( window.position.y + window.size.height ) )
		return false;

	TickType_t time = xTaskGetTickCount();
	TickType_t timeOffset = time - lastTime;

	if( delay.state <= timeOffset )
	{
		int32_t currentSegment = whichSegment( x, y );

		if( segment.state.highlighted == currentSegment )
			segment.state.selected = currentSegment;
		else
			segment.state.highlighted = currentSegment;
	}
	else if( delay.scroll <= timeOffset )
	{
		int32_t tmp = list.size()*segment.size.height - window.size.height;

		if( tmp > 0 )
		{
			offset += lastTouch.y - y;

			if( tmp < offset )
				offset = tmp;
			else if( offset < 0 )
				offset = 0;
		}
	}

	lastTouch.x = x;
	lastTouch.y = y;
	lastTime = time;
	return true;
}

bool FileExplorer::getSelected( FileBase **f ) const
{
	trace_puts( "FileExplorer getSelected" );
	if( ERROR == segment.state.selected )
		return false;

	*f = list.at( segment.state.selected );
	return true;
}

bool FileExplorer::getHighlighted( FileBase **f ) const
{
	trace_puts( "FileExplorer getHighlighted" );
	if( ERROR == segment.state.highlighted )
		return false;

	*f = list.at( segment.state.highlighted );
	return true;
}

FRESULT FileExplorer::scanDirectory( const char *path )
{
	trace_puts( "FileExplorer scanDirectory" );
	FRESULT result;
	DIR dir;
	FILINFO fileInfo;
	FileBase::Attributes attributes;
	FileBase *base;

	result = f_opendir( &dir, path );

	if( FR_OK == result )
	{
		clear();

		for(;;)
		{
			result = f_readdir( &dir, &fileInfo );

			if( result != FR_OK || fileInfo.fname[0] == '\0' )
				break;

			attributes.readOnly = fileInfo.fattrib & AM_RDO;
			attributes.hidden = fileInfo.fattrib & AM_HID;
			attributes.system = fileInfo.fattrib & AM_SYS;
			attributes.archive = fileInfo.fattrib & AM_ARC;

			if( fileInfo.fattrib & AM_DIR )
				base = new Folder( fileInfo.fname, fileInfo.ftime, attributes );
			else
				base = new File( fileInfo.fname, fileInfo.ftime, attributes, fileInfo.fsize );

			list.push_back( base );
		}

		result = f_closedir( &dir );	/* CLose the directory */
	}

	resetSelectAndHighlight();

	return result;
}

bool FileExplorer::isFormat( const File &file, const char **formatTable )
{
	for( size_t i=0; formatTable[i][0] != '\0'; i++ )
		if( 0 == strcmp( file.getFormat()+1, formatTable[i] ) )
			return true;
	return false;
}

bool FileExplorer::isFormat( const File &file, const char *format )
{
	if( 0 == strcmp( file.getFormat()+1, format ) )
		return true;
	return false;
}
