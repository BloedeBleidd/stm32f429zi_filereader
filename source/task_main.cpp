/*
 * task_main.cpp
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "RTOS_includes.h"
#include "task_include.h"
#include "ili9341_ltdc.h"
#include "colors.h"
#include <stdlib.h>
#include "stmpe811.h"
#include "bitmap.h"
#include "button_include.h"
#include "cpu_monitor.h"
#include "file.h"
#include "FAT_includes.h"
#include "file_explorer.h"
#include "led_key.h"
#include "formats.h"

extern bool readerText( Ili9341_LTDC &lcd, const Directory &directory, const File &file );
extern bool readerGraphic( Ili9341_LTDC &lcd, const Directory &directory, const File &file );
extern bool readerAudio( Ili9341_LTDC &lcd, const Directory &directory, const File &file );
extern bool readerVideo( Ili9341_LTDC &lcd, const Directory &directory, const File &file );
extern bool readerUnsupported( Ili9341_LTDC &lcd, const Directory &directory, const File &file );

void drawWindow( Ili9341_LTDC &lcd, ButtonTextBorderFlat<Color,int32_t> &backButton, const FileExplorer &explorer, const Directory &dir );
void scanAndSort( FileExplorer &ex, const Directory &dir, FileExplorer::SortType sort );

extern QueueHandle_t  queueTouchPanel;

void taskMain( void * p )
{
	(void)(p);

	Ili9341_LTDC lcd( Landscape_1 );
	lcd.layerOnBottom();

	FATFS fatFs;
	Directory directory( "1:", '/' );
	FileExplorer explorer( 0, 0, 320, 240, DARKGREY, DARKGREY, BLACK, BLUE, WHITE, Font::Font16x26, 200, 10 );
	ButtonTextBorderFlat<Color,int32_t> backButton( lcd.getWidth()-51, lcd.getHeight()-31, 50, 30, GREEN, RED, BLACK, "BACK", Font::Font11x18, ButtonTextMode::MIDDLE, false );

	f_mount( &fatFs, directory.get(), 1 );
	explorer.scanDirectory( directory.get() );
	explorer.sort( FileExplorer::SortType::NameAscendingFoldersFirst );
	explorer.draw( lcd );
	lcd.refreshCurrentLayer();

	for(;;)
	{
		Stmpe811<int32_t>::Coordinates coord = { 0,0,0 };

		if( pdTRUE == xQueueReceive( queueTouchPanel, ( void * ) &coord, ( TickType_t ) 1 ) )
		{
			if( backButton.isPressed( coord.x, coord.y ) )
			{
				directory.back( 1 );
				scanAndSort( explorer, directory, FileExplorer::SortType::NameAscendingFoldersFirst );
				explorer.reset();
				drawWindow( lcd, backButton, explorer, directory );
			}

			if( true == explorer.refresh( coord.x, coord.y ) )
			{
				FileBase *selected;

				if( true == explorer.getSelected( &selected ) )
				{
					if( FileBase::Folder == selected->getType() )			// change directory
					{
						directory.add( *selected );
						scanAndSort( explorer, directory, FileExplorer::SortType::NameAscendingFoldersFirst );
					}
					else													// open file with external code
					{
						File *file = (File*)(selected);

						if( FileExplorer::isFormat( *file, formatText ) )
							readerText( lcd, directory, *file );
						else if( FileExplorer::isFormat( *file, formatAudio ) )
							readerAudio( lcd, directory, *file );
						else if( FileExplorer::isFormat( *file, formatGraphic ) )
							readerGraphic( lcd, directory, *file );
						else if( FileExplorer::isFormat( *file, formatVideo ) )
							readerVideo( lcd, directory, *file );
						else
							readerUnsupported( lcd, directory, *file );

						explorer.reset();
					}
				}
				else if( true == explorer.getHighlighted( &selected ) )		// show details of file
				{

				}

				drawWindow( lcd, backButton, explorer, directory );
			}

			xQueueReset( queueTouchPanel );
		}
	}
}

void drawWindow( Ili9341_LTDC &lcd, ButtonTextBorderFlat<Color,int32_t> &backButton, const FileExplorer &explorer, const Directory &dir )
{
	explorer.draw( lcd );

	if( dir[2] == dir.getSeparator() )
	{
		backButton.enable();
		backButton.draw( lcd );
	}
	else
		backButton.disable();

	lcd.refreshCurrentLayer();
}

void scanAndSort( FileExplorer &ex, const Directory &dir, FileExplorer::SortType sort )
{
	ex.scanDirectory( dir.get() );
	ex.sort( sort );
}
