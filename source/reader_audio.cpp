/*
 * reader_audio.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "FAT_includes.h"
#include "task_include.h"
#include "gpio.h"
#include "led_key.h"
#include "ili9341_ltdc.h"
#include "stmpe811.h"
#include "colors.h"
#include "file.h"
#include "audio.h"

extern QueueHandle_t  queueTouchPanel;

void formatTo12( uint16_t *buf, uint32_t l )
{
	for( uint32_t i=0; i<l; i++ )
		buf[i] >>= 4;
}

typedef struct
{
	uint8_t channels;
	uint8_t resolution;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint32_t dataSize;
	uint32_t secondsPlayed;
	uint32_t secondsAmount;
}wav_description_t;

bool readerAudio( Ili9341_LTDC &lcd, const Directory &directory, const File &file )
{
	lcd.fill(BLACK);
	lcd.setFont( Font::Font16x26 );
	lcd.drawData( 0, 0, "odtwarzam" , YELLOW );
	lcd.refreshCurrentLayer();

	Directory dir( directory );
	dir.add( file );

	wav_description_t description;
	FIL fil;
	UINT br;
	uint8_t dataSizeBuf[4];

	f_open(&fil,dir.get(),FA_READ);
	f_lseek(&fil,22);
	f_read(&fil,&description.channels, 1, &br);

	f_lseek(&fil,34);
	f_read(&fil,&description.resolution, 1, &br);

	f_lseek(&fil,24);
	f_read(&fil,&description.sampleRate, 4, &br);

	f_lseek(&fil,28);
	f_read(&fil,&description.byteRate, 4, &br);

	f_lseek(&fil,0x4c);
	f_read(&fil,dataSizeBuf, 4, &br);
	description.dataSize = dataSizeBuf[0] | (dataSizeBuf[1]<<8) | (dataSizeBuf[2]<<16) | (dataSizeBuf[3]<<24);

	const uint32_t siz = AUDIO_BUFFER_SIZE;
	AudioData tmp[siz];
	f_lseek(&fil,f_tell(&fil)+siz*sizeof(AudioData));
	f_read(&fil,tmp, siz*sizeof(AudioData), &br);

	AudioChannels c;

	if( description.channels == 1 )
		c = AudioChannels::MONO;
	else
		c = AudioChannels::STEREO;

	audioInit( description.sampleRate, c );
	formatTo12( tmp, siz );
	audioFillBuffer( tmp );
	f_lseek(&fil,f_tell(&fil)+siz*sizeof(AudioData));
	f_read(&fil,tmp, siz*sizeof(AudioData), &br);
	formatTo12( tmp, siz );
	audioFillBuffer( tmp );
	audioStart();

	TickType_t start = xTaskGetTickCount();

	for(;;)
	{
		if( audioIsReady() )
		{
			f_lseek(&fil,f_tell(&fil)+siz*sizeof(AudioData));
			f_read(&fil,tmp, siz*sizeof(AudioData), &br);
			formatTo12( tmp, siz );
			audioFillBuffer( tmp );
		}

		if( br < siz )
			break;

		vTaskDelay( 1 );
	}

	TickType_t stop = xTaskGetTickCount();

	audioReset();

	lcd.drawData( 50, 50, int32_t( stop - start ), YELLOW );
	lcd.refreshCurrentLayer();

	vTaskDelay( 2222 );

	f_close(&fil);
	return true;
}
