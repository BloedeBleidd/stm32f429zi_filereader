/*
 * task_cpu_utility.c
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "RTOS_includes.h"
#include "task_include.h"
#include "cpu_monitor.h"


void taskCpuUtility( void * p )
{
	(void)(p);
	cpuMonitorInit();
	TickType_t lastTime = xTaskGetTickCount();

	for(;;)
	{
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS( CPU_MONITOR_DUTY_TIME ) );
		cpuMonitorWatchDog();
	}
}
