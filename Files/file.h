/*
 * file.h
 *
 *  Created on: Sep 14, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef FILE_H_
#define FILE_H_

#include <cstdlib>
#include <cstdint>

class FileBase
{
public:
	struct Attributes { bool readOnly:1; bool hidden:1; bool archive:1; bool system:1; };
	enum Type { File, Folder };
	typedef uint32_t Time;

	static const Attributes NO_ATTRIBUTES;
	static const char *NONE;

private:
	char *name;
	Type type;
	Time time;
	Attributes attr;

public:
	FileBase( const char *, Type, Time, Attributes );
	FileBase( const FileBase & );
	virtual ~FileBase() = 0;

	virtual FileBase & operator=( const FileBase & );
	FileBase & operator=( const char * );
	virtual bool operator==( const FileBase & ) const;
	char operator[]( size_t num ) const			{ return name[num]; }

	const char * getName() const				{ return name; }
	Type getType() const						{ return type; }
	Time getTime() const						{ return time; }
	Attributes getAttributes() const			{ return attr; }
	bool isReadOnly() const						{ return attr.readOnly; }
	bool isHidden() const						{ return attr.readOnly; }
	bool isArchive() const						{ return attr.archive; }
	bool isSystem() const						{ return attr.system; }

	void setName( const char * );
	void setType( Type t )						{ type = t; }
	void setTime( Time t )						{ time = t; }
	void setAttributes( Attributes a )			{ attr = a; }
	void setAttributeReadOnly( bool ro = true )	{ attr.readOnly = ro; }
	void setAttributeHidden( bool h = true )	{ attr.hidden = h; }
	void setAttributeArchive( bool a = true )	{ attr.archive = a; }
	void setAttributeSystem( bool s = true )	{ attr.system = s; }
};

class File : public FileBase
{
private:
	size_t size;

public:
	File( const char * = NONE, Time = 0, Attributes = NO_ATTRIBUTES, size_t = 0 );
	File( const File & );
	virtual ~File();

	File & operator=( const File & );
	bool operator==( const File & ) const;

	size_t getSize() const						{ return size; }
	const char * getFormat() const;

	void setSize( size_t s )					{ size = s; }
};

class Folder : public FileBase
{
private:

public:
	Folder( const char * = NONE, Time = 0, Attributes = NO_ATTRIBUTES );
	Folder( const Folder & );
	virtual ~Folder();

	Folder & operator=( const Folder & );
	bool operator==( const Folder & ) const;
};

class Directory
{
private:
	static const char DEFAULT_SEPARATOR = '/';
	char *path;
	char separator[2];

public:
	Directory( const char * = "", char = DEFAULT_SEPARATOR );
	Directory( const Directory & );
	Directory( const FileBase &, char = DEFAULT_SEPARATOR );
	~Directory();

	bool operator==( const Directory & ) const;
	char operator[]( size_t num ) const	{ return path[num]; }
	Directory & operator=( const Directory & );
	Directory & operator=( const char * );
	Directory & operator=( const FileBase & );
	Directory & operator+=( const Directory & );
	Directory & operator+=( const char * );
	Directory & operator+=( const FileBase & );
	Directory operator+( const Directory & ) const;
	Directory operator+( const char * ) const;
	Directory operator+( const FileBase & ) const;

	friend Directory operator+( const char *, const Directory & );
	friend Directory operator+( const char *, const FileBase & );
	friend Directory operator+( const Folder &, const FileBase & );

	Directory & operator-=( size_t );
	Directory & operator--();
	friend Directory & operator--( Directory & );

	bool back( size_t = 1 );
	void add( const char * );
	void add( const Directory & );
	void add( const FileBase & );

	const char * get() const					{ return path; };
	char getSeparator() const					{ return separator[0]; }

	void set( const char * );
	void set( const Directory & );
	void set( const FileBase & );
	void setSeparator( char s )					{ separator[0] = s; }

	void reset();
};


#endif /* FILE_H_ */
