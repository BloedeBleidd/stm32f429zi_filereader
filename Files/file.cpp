/*
 * file.cpp
 *
 *  Created on: Sep 14, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "file.h"

#include <cstring>

//--------------------------------------------------constants--------------------------------------------------

const FileBase::Attributes FileBase::NO_ATTRIBUTES = { false, false, false, false };
const char *FileBase::NONE = "none";

//--------------------------------------------------FileBase--------------------------------------------------

FileBase::FileBase( const char *newName, Type typ, Time t, Attributes a )
: type(typ), time(t), attr(a)
{
	name = new char[strlen( newName )+1];
	strcpy( name, newName );
}

FileBase::FileBase( const FileBase &b )
:type(b.type), time( b.time ), attr( b.attr )
{
	name = new char[strlen( b.name )+1];
	strcpy( name, b.name );
}

FileBase::~FileBase()
{
	delete [] name;
}

FileBase & FileBase::operator=( const FileBase &b )
{
	if( this == &b )
		return *this;

	setName( b.name );
	time = b.time;
	attr = b.attr;

	return *this;
}

FileBase & FileBase::operator=( const char *s )
{
	setName( s );
	return *this;
}

bool FileBase::operator==( const FileBase &b ) const
{
	if( this == &b  )
		return true;

	if( time == b.time && type == b.type && 0 == strcmp( name, b.name ) )
		return true;

	return false;
}

void FileBase::setName( const char *newName )
{
	delete [] name;
	name = new char[strlen( newName )+1];
	strcpy( name, newName );
}

//--------------------------------------------------File--------------------------------------------------

File::File( const char *newName, Time t, Attributes a, size_t s )
: FileBase( newName, Type::File, t, a ), size(s)
{

}

File::File( const File &f )
: FileBase( f ), size(f.size)
{

}

File::~File()
{

}

File & File::operator=( const File &f )
{
	if( this == &f )
		return *this;

	*this = f;
	size = f.size;

	return *this;
}

bool File::operator==( const File &f ) const
{
	if( size != f.size )
		return false;
	else
		return FileBase::operator==( f );
}

const char * File::getFormat() const
{
	return strrchr( getName(), '.' );
}

//--------------------------------------------------Folder--------------------------------------------------

Folder::Folder( const char *newName, Time t, Attributes a )
: FileBase( newName, Type::Folder, t, a )
{

}

Folder::Folder( const Folder &f )
: FileBase( f )
{

}

Folder::~Folder()
{

}

bool Folder::operator==( const Folder &f ) const
{
	return FileBase::operator==( f );
}

//--------------------------------------------------Directory--------------------------------------------------

Directory::Directory( const char *newPath, char s )
{
	path = new char[strlen( newPath )+1];
	strcpy( path, newPath );
	separator[0] = s;
	separator[1] = '\0';
}

Directory::Directory( const Directory &d )
{
	path = new char[strlen( d.path )+1];
	strcpy( path, d.path );
	separator[0] = d.separator[0];
	separator[1] = '\0';
}

Directory::Directory( const FileBase &b, char s )
{
	path = new char[strlen( b.getName() )+1];
	strcpy( path, b.getName() );
	separator[0] = s;
	separator[1] = '\0';
}

Directory::~Directory()
{
	delete [] path;
}

bool Directory::operator==( const Directory &d ) const
{
	if( 0 == strcmp( path, d.path ) )
		return true;
	else
		return false;
}

Directory & Directory::operator=( const Directory &d )
{
	if( this == &d )
		return *this;

	set( d.path );

	return *this;
}

Directory & Directory::operator=( const char *newPath )
{
	set( newPath );

	return *this;
}

Directory & Directory::operator=( const FileBase &b )
{
	set( b.getName() );

	return *this;
}

Directory & Directory::operator+=( const Directory &d )
{
	add( d.path );

	return *this;
}

Directory & Directory::operator+=( const char *newPath )
{
	add( newPath );

	return *this;
}

Directory & Directory::operator+=( const FileBase &b )
{
	add( b.getName() );

	return *this;
}

Directory Directory::operator+( const Directory &d ) const
{
	Directory tmp( *this );
	tmp = tmp + separator + d;

	return tmp;
}

Directory Directory::operator+( const char *newPath ) const
{
	char *tmpS = new char[ strlen( this->path ) + 1 + strlen( newPath ) + 1 ];
	strcpy( tmpS, this->get() );
	strcat( tmpS, separator );
	strcat( tmpS, newPath );
	Directory tmp( tmpS );
	delete [] tmpS;

	return tmp;
}

Directory Directory::operator+( const FileBase &b ) const
{
	Directory tmp( *this );
	tmp = tmp + separator + b;

	return tmp;
}

Directory operator+( const char *p, const Directory &d )
{
	Directory tmp( p );
	tmp = tmp + d.separator + d;

	return tmp;
}

Directory operator+( const char *p, const FileBase &b )
{
	Directory tmp( p );
	tmp = tmp + tmp.separator + b;

	return tmp;
}

Directory operator+( const Folder &f, const FileBase &b )
{
	Directory tmp( f );
	tmp = tmp + tmp.separator + f;

	return tmp;
}

Directory & Directory::operator-=( size_t n )
{
	back( n );

	return *this;
}

Directory & Directory::operator--()
{
	back();

	return *this;
}

Directory & operator--( Directory &d )
{
	d.back();

	return d;
}

void Directory::add( const char *nextDir )
{
	char *tmp = new char[strlen( path )+strlen( nextDir )+2];
	strcpy( tmp, path );
	delete [] path;
	strcat( tmp, separator );
	strcat( tmp, nextDir );
	path = tmp;
}

void Directory::add( const Directory &d )
{
	add( d.path );
}

void Directory::add( const FileBase &b )
{
	add( b.getName() );
}

bool Directory::back( size_t n )
{
	char *pos;

	for( size_t i=0; i<n; i++, *pos='\0' )
		if( nullptr == ( pos = strrchr( path, separator[0] ) ) )
			break;

	char *tmpPath = new char[strlen( path )+1];
	strcpy( tmpPath, path );
	delete [] path;
	path = tmpPath;

	return true;
}

void Directory::set( const char *newPath )
{
	delete [] path;
	path = new char[strlen( newPath )+1];
	strcpy( path, newPath );
}

void Directory::set( const Directory &d )
{
	set( d.path );
}

void Directory::set( const FileBase &b )
{
	set( b.getName() );
}

void Directory::reset()
{
	delete [] path;
}

