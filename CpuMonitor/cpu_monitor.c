/*
 * cpu_monitor.c
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "cpu_monitor.h"
#include "led_key.h"
#include "gpio.h"
#include "RTOS_includes.h"
#include <stdlib.h>
#include <string.h>

static const uint16_t TICKS_FOR_DUTY = 30000;
static float cpuHistory[CPU_MONITOR_HISTORY_LENGHT];
static int32_t position = CPU_MONITOR_HISTORY_LENGHT-1;

void cpuMonitorInit(void)
{
	gpioPinConfiguration( LED_RED_GPIO, LED_RED_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_MEDIUM_SPEED );

	RCC->APB1ENR = RCC_APB1ENR_TIM14EN;
	TIM14->PSC = SystemCoreClock/2/TICKS_FOR_DUTY/(configTICK_RATE_HZ/CPU_MONITOR_DUTY_TIME)-1;
	TIM14->ARR = 0xffff;
}

void cpuMonitorStart(void)
{
	TIM14->CR1 = TIM_CR1_CEN;
	gpioBitSet( LED_RED_GPIO, LED_RED_PIN );
}

void cpuMonitorStop(void)
{
	gpioBitReset( LED_RED_GPIO, LED_RED_PIN );
	TIM14->CR1 = 0;
}

void cpuMonitorWatchDog(void)
{
	const float MUL = (float)100/(float)TICKS_FOR_DUTY;
	const float MAX_VAL = 100;

	TIM14->CR1 = 0;
	float tmp = TIM14->CNT;
	TIM14->EGR = TIM_EGR_UG;
	TIM14->CR1 = TIM_CR1_CEN;

	tmp = tmp*MUL;

	if( tmp > MAX_VAL )
		tmp = MAX_VAL;

	position--;
	if( position < 0 )
		position = CPU_MONITOR_HISTORY_LENGHT-1;

	*(cpuHistory+position) = tmp;
}

float cpuMonitorGetValue( size_t index )
{
	if( index >= CPU_MONITOR_HISTORY_LENGHT )
		return 0;

	return *(cpuHistory+( (position+index)%CPU_MONITOR_HISTORY_LENGHT ));
}

void cpuMonitorGetHistory( float *data, size_t len )
{
	if( len > CPU_MONITOR_HISTORY_LENGHT )
		len = CPU_MONITOR_HISTORY_LENGHT;

	vTaskSuspendAll();

	if( position+len >= CPU_MONITOR_HISTORY_LENGHT )
	{
		memcpy( data, &cpuHistory[position], sizeof(float)*(CPU_MONITOR_HISTORY_LENGHT-position) );
		memcpy( data+CPU_MONITOR_HISTORY_LENGHT-position, cpuHistory, sizeof(float)*(len+position-CPU_MONITOR_HISTORY_LENGHT) );
	}
	else
	{
		memcpy( data, &cpuHistory[position], sizeof(float)*len );
	}

	xTaskResumeAll();
}
