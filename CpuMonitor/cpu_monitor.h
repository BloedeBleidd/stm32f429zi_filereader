/*
 * cpu_monitor.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef CPUMONITOR_CPU_MONITOR_H_
#define CPUMONITOR_CPU_MONITOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

#define CPU_MONITOR_DUTY_TIME		100
#define CPU_MONITOR_HISTORY_LENGHT	60*1000/CPU_MONITOR_DUTY_TIME

void cpuMonitorInit(void);
void cpuMonitorStart(void);
void cpuMonitorStop(void);
void cpuMonitorWatchDog(void);
float cpuMonitorGetValue( size_t index );
void cpuMonitorGetHistory( float *data, size_t len );

#ifdef __cplusplus
}
#endif

#endif /* CPUMONITOR_CPU_MONITOR_H_ */
