/*
 * mmc.c
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"
#include "gpio.h"
#include "stm32f4xx.h"
#include "diskio.h"
#include "mmc.h"

/* MMC card type flags (MMC_GET_TYPE) */
#define CT_MMC		0x01		/* MMC ver 3 */
#define CT_SD1		0x02		/* SD ver 1 */
#define CT_SD2		0x04		/* SD ver 2 */
#define CT_SDC		(CT_SD1|CT_SD2)	/* SD */
#define CT_BLOCK	0x08		/* Block addressing */

/* MMC/SD command */
#define CMD0	(0)			/* GO_IDLE_STATE */
#define CMD1	(1)			/* SEND_OP_COND (MMC) */
#define	ACMD41	(0x80+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(8)			/* SEND_IF_COND */
#define CMD9	(9)			/* SEND_CSD */
#define CMD10	(10)		/* SEND_CID */
#define CMD12	(12)		/* STOP_TRANSMISSION */
#define ACMD13	(0x80+13)	/* SD_STATUS (SDC) */
#define CMD16	(16)		/* SET_BLOCKLEN */
#define CMD17	(17)		/* READ_SINGLE_BLOCK */
#define CMD18	(18)		/* READ_MULTIPLE_BLOCK */
#define CMD23	(23)		/* SET_BLOCK_COUNT (MMC) */
#define	ACMD23	(0x80+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(24)		/* WRITE_BLOCK */
#define CMD25	(25)		/* WRITE_MULTIPLE_BLOCK */
#define CMD32	(32)		/* ERASE_ER_BLK_START */
#define CMD33	(33)		/* ERASE_ER_BLK_END */
#define CMD38	(38)		/* ERASE */
#define CMD55	(55)		/* APP_CMD */
#define CMD58	(58)		/* READ_OCR */

typedef enum { Transmit, Receive } Xmit_t;

static volatile DSTATUS Stat = STA_NOINIT;	/* Physical drive status */
static BYTE CardType;						/* Card type flags */

/*-----------------------------------------------------------------------*/
/* SPI controls (Platform dependent)                                     */
/*-----------------------------------------------------------------------*/

#define USE_DMA			2			// 0 = OFF , 1 = DMA , 2 = DMA+INTERRUPT

#define DMA				DMA1
#define DMA_TX_STATUS	DMA->LISR
#define DMA_RX_STATUS	DMA->LISR
#define DMA_STREAM_TX	DMA1_Stream5 // DMA1_Stream7
#define DMA_STREAM_RX	DMA1_Stream0 // DMA1_Stream2

#define SPIx 			SPI3

#define CLK_GPIO 		GPIOC
#define CLK_PIN			10

#define MISO_GPIO 		GPIOC
#define MISO_PIN		11

#define MOSI_GPIO 		GPIOC
#define MOSI_PIN		12

#define CS_GPIO 		GPIOA
#define CS_PIN			15

#define SPI_PRESCALLER	(SPI_CR1_BR_1)

/* Prescallers table
 * 2   == (0x00)
 * 4   == (SPI_CR1_BR_0)
 * 8   == (SPI_CR1_BR_1)
 * 16  == (SPI_CR1_BR_0 | SPI_CR1_BR_1)
 * 32  == (SPI_CR1_BR_2)
 * 64  == (SPI_CR1_BR_0 | SPI_CR1_BR_2)
 * 128 == (SPI_CR1_BR_1 | SPI_CR1_BR_2)
 * 256 == (SPI_CR1_BR)
 */

/*
static inline void waitForBusyFlag()	{ while(!(SPIx->SR & SPI_SR_BSY)) {}; }
static inline void setSpi16BitMode()	{ SPIx->CR1 &= ~SPI_CR1_SPE; SPIx->CR1 |= (SPI_CR1_DFF | SPI_CR1_SPE); }
static inline void setSpi8BitMode()		{ SPIx->CR1 &= ~(SPI_CR1_DFF | SPI_CR1_SPE); SPIx->CR1 |= SPI_CR1_SPE; }
*/

static inline void csHigh()				{ CS_GPIO->ODR |= (1<<CS_PIN); }
static inline void csLow()				{ CS_GPIO->ODR &= ~(1<<CS_PIN); }
static inline void fclkSlow()			{ SPIx->CR1 = (SPIx->CR1 & ~0x38) | SPI_CR1_BR; } /* Set SCLK = PCLK / 256 */
static inline void fclkFast()			{ SPIx->CR1 = (SPIx->CR1 & ~0x38) | SPI_PRESCALLER; }
static inline void waitForTxeFlag()		{ while(!(SPIx->SR & SPI_SR_TXE)) {}; }
static inline void waitForRxeFlag()		{ while(!(SPIx->SR & SPI_SR_RXNE)) {}; }

#if USE_DMA == 2
	SemaphoreHandle_t semaphoreMMC;
#endif

/* Initialize SD interface */
static void initGpio (void)
{
	gpioPinConfiguration(CLK_GPIO, CLK_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
	gpioPinConfiguration(MISO_GPIO, MISO_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
	gpioPinConfiguration(MOSI_GPIO, MOSI_PIN, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
	gpioPinAlternateFunctionConfiguration(CLK_GPIO, CLK_PIN, 6);
	gpioPinAlternateFunctionConfiguration(MISO_GPIO, MISO_PIN, 6);
	gpioPinAlternateFunctionConfiguration(MOSI_GPIO, MOSI_PIN, 6);
	gpioPinConfiguration(CS_GPIO, CS_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_VERY_HIGH_SPEED);
}

static void initSpi (void)
{
	RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
	__DSB();
	#if USE_DMA > 0
		SPIx->CR2 |= SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN;
	#endif
	SPIx->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_CPHA | SPI_CR1_CPOL;
	SPIx->CR1 |= SPI_CR1_SPE;
	csHigh();
}

#if USE_DMA > 0
	static void initDma (void)
	{
		RCC->AHB1ENR |= RCC_AHB1ENR_DMA1EN;
		__DSB();
		DMA_STREAM_TX->PAR = (uint32_t)&SPIx->DR;
		DMA_STREAM_TX->CR |= DMA_SxCR_DIR_0;
		DMA_STREAM_RX->PAR = (uint32_t)&SPIx->DR;

		#if USE_DMA == 2
			NVIC_SetPriority( DMA1_Stream0_IRQn, 10 );
			NVIC_EnableIRQ( DMA1_Stream0_IRQn );
		#endif
	}

	static void clearDmaFlags (void)
	{
		DMA->LIFCR = DMA_LIFCR_CTCIF0;
		DMA->HIFCR = DMA_HIFCR_CTCIF5;
	}

	static void dmaXmitBlock( const Xmit_t dir, const BYTE *buff, UINT length )
	{
		uint32_t dummy = 0xFFFFFFFF;

		// size of payload
		DMA_STREAM_RX->NDTR = length;
		DMA_STREAM_TX->NDTR = length;

		if( Transmit == dir )
		{
			// base 0 memory address - receiving is irrelevant, transmitting real data
			DMA_STREAM_RX->M0AR = (uint32_t)&dummy;
			DMA_STREAM_TX->M0AR = (uint32_t)buff;
			// disable incrementing receive buffer, enable transmitting
			DMA_STREAM_RX->CR &= ~DMA_SxCR_MINC;
			DMA_STREAM_TX->CR |= DMA_SxCR_MINC;
			// enable interrupt after finished reception
			DMA_STREAM_TX->CR |= DMA_SxCR_TCIE;
		}
		else
		{
			// base 0 memory address - receiving real data, sending dummy payload
			DMA_STREAM_RX->M0AR = (uint32_t)buff;
			DMA_STREAM_TX->M0AR = (uint32_t)&dummy;
			// enable incrementing receive buffer, disable transmitting (one dummy byte)
			DMA_STREAM_RX->CR |= DMA_SxCR_MINC;
			DMA_STREAM_TX->CR &= ~DMA_SxCR_MINC;
			// enable interrupt after finished reception
			DMA_STREAM_RX->CR |= DMA_SxCR_TCIE;
		}

		// enable both streams
		DMA_STREAM_RX->CR |= DMA_SxCR_EN;
		DMA_STREAM_TX->CR |= DMA_SxCR_EN;
		while ((DMA_STREAM_RX->CR & DMA_SxCR_EN) == 0) {};
		while ((DMA_STREAM_TX->CR & DMA_SxCR_EN) == 0) {};

		// issue transmission start
		SPIx->CR2 |= SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN;

		// wait for transmission to end - non blocking
		#if USE_DMA == 1
			while( (DMA->LISR & DMA_LISR_TCIF0) == 0 )		{ vTaskDelay( pdMS_TO_TICKS(1) ); }
		#else
			xSemaphoreTake( semaphoreMMC, portMAX_DELAY );
		#endif

		// wait until both streams disable themselves
		while ((DMA_STREAM_RX->CR & DMA_SxCR_EN) != 0) {};
		while ((DMA_STREAM_TX->CR & DMA_SxCR_EN) != 0) {};

		// disable transaction request
		SPIx->CR2 &= ~(SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN);

		// clear flags for DMA allowing enabling streams in the future
		clearDmaFlags();
	}

#else
	/* Receive multiple byte */
	static void receiveMulti
	(
		BYTE *p, 	// Data buffer
		UINT cnt 	// Size of data block
	)
	{
		do
		{
			waitForTxeFlag();
			SPIx->DR = 0xFF;
			waitForRxeFlag();
			*p++ = SPIx->DR;

			waitForTxeFlag();
			SPIx->DR = 0xFF;
			waitForRxeFlag();
			*p++ = SPIx->DR;
		} while (cnt -= 2);
	}

	#if _USE_WRITE
		/* Send multiple byte */
		static void transmitMulti
		(
			const BYTE *p,	// Data block to be sent
			UINT cnt		// Size of data block
		)
		{
			do
			{
				waitForTxeFlag();
				SPIx->DR = *p++;
				waitForRxeFlag();
				SPIx->DR;
				waitForTxeFlag();
				SPIx->DR = *p++;
				waitForRxeFlag();
				SPIx->DR;
			} while (cnt -= 2);
		}
	#endif
#endif

/* Interrupts */
#if USE_DMA == 2
	void DMA1_Stream0_IRQHandler (void)
	{
		portBASE_TYPE contextSwitch = pdFALSE;

		if( (DMA->LISR & DMA_LISR_TCIF0) != 0 )
		{
			xSemaphoreGiveFromISR( semaphoreMMC, &contextSwitch );
			clearDmaFlags();
		}

		portEND_SWITCHING_ISR(contextSwitch);
	}
#endif

/* Exchange a byte */
static BYTE xchangeSpiSingle (BYTE dat)
{
	waitForTxeFlag();
	SPIx->DR = dat;
	waitForRxeFlag();
	return (BYTE)SPIx->DR;
}

/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/

static int wait_ready (	/* 1:Ready, 0:Timeout */
	UINT wt			/* Timeout [ms] */
)
{
	BYTE d = xchangeSpiSingle(0xFF);
	UINT timer = wt;

	while (d != 0xFF && timer) {	/* Wait for card goes ready or timeout */
		vTaskDelay( pdMS_TO_TICKS(1) );
		timer--;
		d = xchangeSpiSingle(0xFF);
	}

	return (d == 0xFF) ? 1 : 0;
}

/*-----------------------------------------------------------------------*/
/* Deselect card and release SPI                                         */
/*-----------------------------------------------------------------------*/

static void deselect (void)
{
	csHigh();	/* Set CS# high */
	xchangeSpiSingle(0xFF);	/* Dummy clock (force DO hi-z for multiple slave SPI) */
}

/*-----------------------------------------------------------------------*/
/* Select card and wait for ready                                        */
/*-----------------------------------------------------------------------*/

static int select (void)	/* 1:OK, 0:Timeout */
{
	csLow();		/* Set CS# low */
	xchangeSpiSingle(0xFF);	/* Dummy clock (force DO enabled) */
	if (wait_ready(50))
		return 1;	/* Wait for card ready */

	deselect();
	return 0;	/* Timeout */
}

/*-----------------------------------------------------------------------*/
/* Receive a data packet from the MMC                                    */
/*-----------------------------------------------------------------------*/

static int rcvr_datablock (	/* 1:OK, 0:Error */
	BYTE *buff,				/* Data buffer */
	UINT btr				/* Data block length (byte) */
)
{
	BYTE token;

	UINT timer = 200;
	token = xchangeSpiSingle(0xFF);

	while ((token == 0xFF) && timer) {	/* Wait for DataStart token in timeout of 200ms */
		vTaskDelay( pdMS_TO_TICKS(1) );
		timer--;
		token = xchangeSpiSingle(0xFF);
	}

	if(token != 0xFE)
		return 0;		/* Function fails if invalid DataStart token or timeout */

	/* Store trailing data to the buffer */
	#if USE_DMA == 0
		receiveMulti( buff, btr );
	#else
		dmaXmitBlock( Receive, buff, btr );
	#endif

	xchangeSpiSingle(0xFF); xchangeSpiSingle(0xFF);			/* Discard CRC */

	return 1;						/* Function succeeded */
}

/*-----------------------------------------------------------------------*/
/* Send a data packet to the MMC                                         */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
	static int xmit_datablock (	/* 1:OK, 0:Failed */
		const BYTE *buff,	/* Ponter to 512 byte data to be sent */
		BYTE token			/* Token */
	)
	{
		BYTE resp;

		if (!wait_ready(50))
			return 0;		/* Wait for card ready */

		xchangeSpiSingle(token);					/* Send token */
		if (token != 0xFD)
		{				/* Send data if token is other than StopTran */
			#if USE_DMA == 0
				transmitMulti(buff, 512);
			#else
				dmaXmitBlock( Transmit, buff, 512 );
			#endif

			xchangeSpiSingle(0xFF); xchangeSpiSingle(0xFF);	/* Dummy CRC */
			resp = xchangeSpiSingle(0xFF);				/* Receive data resp */
			if ((resp & 0x1F) != 0x05)		/* Function fails if the data packet was not accepted */
				return 0;
		}
		return 1;
	}
#endif

/*-----------------------------------------------------------------------*/
/* Send a command packet to the MMC                                      */
/*-----------------------------------------------------------------------*/

static BYTE send_cmd (	/* Return value: R1 resp (bit7==1:Failed to send) */
	BYTE cmd,			/* Command index */
	DWORD arg			/* Argument */
)
{
	BYTE n, res;

	if (cmd & 0x80)
	{	/* Send a CMD55 prior to ACMD<n> */
		cmd &= 0x7F;
		res = send_cmd(CMD55, 0);
		if (res > 1)
			return res;
	}

	/* Select the card and wait for ready except to stop multiple block read */
	if (cmd != CMD12)
	{
		deselect();
		if (!select())
			return 0xFF;
	}

	/* Send command packet */
	xchangeSpiSingle(0x40 | cmd);				/* Start + command index */
	xchangeSpiSingle((BYTE)(arg >> 24));		/* Argument[31..24] */
	xchangeSpiSingle((BYTE)(arg >> 16));		/* Argument[23..16] */
	xchangeSpiSingle((BYTE)(arg >> 8));			/* Argument[15..8] */
	xchangeSpiSingle((BYTE)arg);				/* Argument[7..0] */
	n = 0x01;							/* Dummy CRC + Stop */
	if (cmd == CMD0) n = 0x95;			/* Valid CRC for CMD0(0) */
	if (cmd == CMD8) n = 0x87;			/* Valid CRC for CMD8(0x1AA) */
	xchangeSpiSingle(n);

	/* Receive command resp */
	if (cmd == CMD12) xchangeSpiSingle(0xFF);	/* Diacard following one byte when CMD12 */
	n = 10;								/* Wait for response (10 bytes max) */
	do
		res = xchangeSpiSingle(0xFF);
	while ((res & 0x80) && --n);

	return res;							/* Return received response */
}

/*--------------------------------------------------------------------------

   Public Functions

---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Initialize disk drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS MMC_disk_initialize ()
{
	BYTE n, cmd, ty, ocr[4];

	initGpio();		/* Initialize GPIO */
	initSpi();		/* Initialize SPI */
	#if USE_DMA > 0
		initDma();		/* Initialize DMA */
	#endif

	#if USE_DMA == 2
		static BYTE firstInit = 1;
		if( firstInit == 1 )
		{
			firstInit = 0;
			semaphoreMMC = xSemaphoreCreateBinary();
		}
	#endif

	if (Stat & STA_NODISK) return Stat;	/* Is card existing in the soket? */

	fclkSlow();
	for (n = 10; n; n--) xchangeSpiSingle(0xFF);	/* Send 80 dummy clocks */

	ty = 0;
	if (send_cmd(CMD0, 0) == 1)
	{			/* Put the card SPI/Idle state */
		UINT timer = 500;						/* Initialization timeout = 0,5 sec */
		if (send_cmd(CMD8, 0x1AA) == 1)
		{	/* SDv2? */
			for (n = 0; n < 4; n++) ocr[n] = xchangeSpiSingle(0xFF);	/* Get 32 bit return value of R7 resp */
			if (ocr[2] == 0x01 && ocr[3] == 0xAA)
			{				/* Is the card supports vcc of 2.7-3.6V? */
				while (timer && send_cmd(ACMD41, 1UL << 30))	/* Wait for end of initialization with ACMD41(HCS) */
				{
					vTaskDelay( pdMS_TO_TICKS(1) );
					timer--;
				}
				if (timer && send_cmd(CMD58, 0) == 0)
				{		/* Check CCS bit in the OCR */
					for (n = 0; n < 4; n++) ocr[n] = xchangeSpiSingle(0xFF);
					ty = (ocr[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	/* Card id SDv2 */
				}
			}
		}
		else
		{	/* Not SDv2 card */
			if (send_cmd(ACMD41, 0) <= 1)
			{	/* SDv1 or MMC? */
				ty = CT_SD1;
				cmd = ACMD41;	/* SDv1 (ACMD41(0)) */
			}
			else
			{
				ty = CT_MMC;
				cmd = CMD1;	/* MMCv3 (CMD1(0)) */
			}
			while (timer && send_cmd(cmd, 0)) 		/* Wait for end of initialization */
			{
				vTaskDelay( pdMS_TO_TICKS(1) );
				timer--;
			}
			if (!timer || send_cmd(CMD16, 512) != 0)	/* Set block length: 512 */
				ty = 0;
		}
	}

	CardType = ty;	/* Card type */
	deselect();

	if (ty)
	{			/* OK */
		fclkFast();		/* Set fast clock */
		Stat &= ~STA_NOINIT;	/* Clear STA_NOINIT flag */
	}
	else 			/* Failed */
		Stat = STA_NOINIT;

	return Stat;
}

/*-----------------------------------------------------------------------*/
/* Get disk status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS MMC_disk_status ()
{
	return Stat;	/* Return disk status */
}

/*-----------------------------------------------------------------------*/
/* Read sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT MMC_disk_read (
	BYTE *buff,		/* Pointer to the data buffer to store read data */
	DWORD sector,	/* Start sector number (LBA) */
	UINT count		/* Number of sectors to read (1..128) */
)
{
	if (!count) return RES_PARERR;				/* Check parameter */
	if (Stat & STA_NOINIT) return RES_NOTRDY;	/* Check if drive is ready */

	if (!(CardType & CT_BLOCK)) sector *= 512;	/* LBA ot BA conversion (byte addressing cards) */

	if (count == 1)
	{	/* Single sector read */
		if ((send_cmd(CMD17, sector) == 0)	/* READ_SINGLE_BLOCK */
			&& rcvr_datablock(buff, 512))
			count = 0;
	}
	else
	{				/* Multiple sector read */
		if (send_cmd(CMD18, sector) == 0)
		{	/* READ_MULTIPLE_BLOCK */
			do {
				if (!rcvr_datablock(buff, 512)) break;
				buff += 512;
			} while (--count);
			send_cmd(CMD12, 0);				/* STOP_TRANSMISSION */
		}
	}
	deselect();

	return count ? RES_ERROR : RES_OK;	/* Return result */
}

/*-----------------------------------------------------------------------*/
/* Write sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
	DRESULT MMC_disk_write (
		const BYTE *buff,	/* Ponter to the data to write */
		DWORD sector,		/* Start sector number (LBA) */
		UINT count			/* Number of sectors to write (1..128) */
	)
	{
		if (!count) return RES_PARERR;				/* Check parameter */
		if (Stat & STA_NOINIT) return RES_NOTRDY;	/* Check drive status */
		if (Stat & STA_PROTECT) return RES_WRPRT;	/* Check write protect */

		if (!(CardType & CT_BLOCK)) sector *= 512;	/* LBA ==> BA conversion (byte addressing cards) */

		if (count == 1)
		{	/* Single sector write */
			if ((send_cmd(CMD24, sector) == 0)	/* WRITE_BLOCK */
				&& xmit_datablock(buff, 0xFE))
				count = 0;
		}
		else
		{				/* Multiple sector write */
			if (CardType & CT_SDC) send_cmd(ACMD23, count);	/* Predefine number of sectors */
			if (send_cmd(CMD25, sector) == 0)
			{	/* WRITE_MULTIPLE_BLOCK */
				do {
					if (!xmit_datablock(buff, 0xFC)) break;
					buff += 512;
				} while (--count);
				if (!xmit_datablock(0, 0xFD))	/* STOP_TRAN token */
					count = 1;
			}
		}
		deselect();

		return count ? RES_ERROR : RES_OK;	/* Return result */
	}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous drive controls other than data read/write               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
	DRESULT MMC_disk_ioctl (
		BYTE pdrv,		/* Physical drive nmuber (0..) */
		BYTE cmd,		/* Control command code */
		void *buff		/* Pointer to the conrtol data */
	)
	{
		DRESULT res;
		BYTE n, csd[16];
		DWORD *dp, st, ed, csize;


		if (Stat & STA_NOINIT) return RES_NOTRDY;	/* Check if drive is ready */

		res = RES_ERROR;

		switch (cmd)
		{
		case CTRL_SYNC :		/* Wait for end of internal write process of the drive */
			if (select()) res = RES_OK;
			break;

		case GET_SECTOR_COUNT :	/* Get drive capacity in unit of sector (DWORD) */
			if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16))
			{
				if ((csd[0] >> 6) == 1)
				{	/* SDC ver 2.00 */
					csize = csd[9] + ((WORD)csd[8] << 8) + ((DWORD)(csd[7] & 63) << 16) + 1;
					*(DWORD*)buff = csize << 10;
				}
				else
				{					/* SDC ver 1.XX or MMC ver 3 */
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					csize = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(DWORD*)buff = csize << (n - 9);
				}
				res = RES_OK;
			}
			break;

		case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */
			if (CardType & CT_SD2) {	/* SDC ver 2.00 */
				if (send_cmd(ACMD13, 0) == 0)
				{	/* Read SD status */
					xchangeSpiSingle(0xFF);
					if (rcvr_datablock(csd, 16))
					{				/* Read partial block */
						for (n = 64 - 16; n; n--) xchangeSpiSingle(0xFF);	/* Purge trailing data */
						*(DWORD*)buff = 16UL << (csd[10] >> 4);
						res = RES_OK;
					}
				}
			}
			else
			{					/* SDC ver 1.XX or MMC */
				if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16))
				{	/* Read CSD */
					if (CardType & CT_SD1)
					{	/* SDC ver 1.XX */
						*(DWORD*)buff = (((csd[10] & 63) << 1) + ((WORD)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
					}
					else
					{					/* MMC */
						*(DWORD*)buff = ((WORD)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
					}
					res = RES_OK;
				}
			}
			break;

		case CTRL_TRIM :	/* Erase a block of sectors (used when _USE_ERASE == 1) */
			if (!(CardType & CT_SDC)) break;				/* Check if the card is SDC */
			if (disk_ioctl(DEV_MMC, MMC_GET_CSD, csd)) break;	/* Get CSD */
			if (!(csd[0] >> 6) && !(csd[10] & 0x40)) break;	/* Check if sector erase can be applied to the card */
			dp = buff; st = dp[0]; ed = dp[1];				/* Load sector block */
			if (!(CardType & CT_BLOCK))
			{
				st *= 512; ed *= 512;
			}
			if (send_cmd(CMD32, st) == 0 && send_cmd(CMD33, ed) == 0 && send_cmd(CMD38, 0) == 0 && wait_ready(3000))	/* Erase sector block */
				res = RES_OK;	/* FatFs does not check result of this command */
			break;

		default:
			res = RES_PARERR;
		}

		deselect();

		return res;
	}
#endif
