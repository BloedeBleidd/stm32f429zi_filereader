/*
 * mmc.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef MMC_H_
#define MMC_H_

#include "diskio.h"


DSTATUS MMC_disk_status ();
DSTATUS MMC_disk_initialize ();
DRESULT MMC_disk_read
(
	BYTE *buff,		/* Pointer to the data buffer to store read data */
	DWORD sector,	/* Start sector number (LBA) */
	UINT count		/* Number of sectors to read (1..128) */
);
DRESULT MMC_disk_write
(
	const BYTE *buff,	/* Ponter to the data to write */
	DWORD sector,		/* Start sector number (LBA) */
	UINT count			/* Number of sectors to write (1..128) */
);
DRESULT MMC_disk_ioctl
(
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control command code */
	void *buff		/* Pointer to the conrtol data */
);

#endif /* MMC_H_ */
