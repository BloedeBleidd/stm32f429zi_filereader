/*
 * usb.c
 *
 *  Created on: Sep 14, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "FreeRTOS.h"
#include "timers.h"
#include "semphr.h"
#include "gpio.h"
#include "stm32f4xx.h"
#include "diskio.h"
#include "usb.h"

static volatile DSTATUS Stat = STA_NOINIT;	/* Physical drive status */


static void initGpio (void)
{

}

static void initUsb (void)
{

}

DSTATUS USB_disk_initialize ()
{
	initGpio();
	initUsb();

	return Stat;
}

DSTATUS USB_disk_status ()
{
	return Stat;
}

DRESULT USB_disk_read (
	BYTE *buff,		/* Pointer to the data buffer to store read data */
	DWORD sector,	/* Start sector number (LBA) */
	UINT count		/* Number of sectors to read (1..128) */
)
{

	return RES_PARERR;
}

DRESULT USB_disk_write (
	const BYTE *buff,	/* Ponter to the data to write */
	DWORD sector,		/* Start sector number (LBA) */
	UINT count			/* Number of sectors to write (1..128) */
)
{

	return RES_PARERR;
}

DRESULT USB_disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control command code */
	void *buff		/* Pointer to the conrtol data */
)
{
	return RES_PARERR;
}
