/*
 * usb.h
 *
 *  Created on: Sep 14, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef USB_USB_H_
#define USB_USB_H_


#include "diskio.h"


DSTATUS USB_disk_status ();
DSTATUS USB_disk_initialize ();
DRESULT USB_disk_read
(
	BYTE *buff,		/* Pointer to the data buffer to store read data */
	DWORD sector,	/* Start sector number (LBA) */
	UINT count		/* Number of sectors to read (1..128) */
);
DRESULT USB_disk_write
(
	const BYTE *buff,	/* Ponter to the data to write */
	DWORD sector,		/* Start sector number (LBA) */
	UINT count			/* Number of sectors to write (1..128) */
);
DRESULT USB_disk_ioctl
(
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control command code */
	void *buff		/* Pointer to the conrtol data */
);


#endif /* USB_USB_H_ */
