/*
 * rtc.c
 *
 *  Created on: Sep 14, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "rtc.h"
#include "integer.h"

/*---------------------------------------------------------*/
/* User Provided Timer Function for FatFs module           */
/*---------------------------------------------------------*/
/* This is a real time clock service to be called from     */
/* FatFs module. Any valid time must be returned even if   */
/* the system does not support a real time clock.          */

DWORD get_fattime (void)
{
    return  ((2011UL-1980) << 25)    // Year = 2011
            | (1UL << 21)            // Month = January
            | (1UL << 16)            // Day = 1
            | (12U << 11)            // Hour = 12
            | (0U << 5)              // Min = 00
            | (0U >> 1)              // Sec = 00
            ;
}


