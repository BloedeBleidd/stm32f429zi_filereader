
#include "stm32f4xx.h"
#include "sdram.h"

#include "gpio.h"

#define TMRD(x) (x << FMC_SDTR1_TMRD_Pos)
#define TXSR(x) (x << FMC_SDTR1_TXSR_Pos)
#define TRAS(x) (x << FMC_SDTR1_TRAS_Pos)
#define TRC(x)  (x << FMC_SDTR1_TRC_Pos)
#define TWR(x)  (x << FMC_SDTR1_TWR_Pos)
#define TRP(x)  (x << FMC_SDTR1_TRP_Pos)
#define TRCD(x) (x << FMC_SDTR1_TRCD_Pos)

static  GPIO_TypeDef * const GPIOInitTable[] =
{
		GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOF, GPIOG, GPIOG,
		GPIOD, GPIOD, GPIOD, GPIOD, GPIOE, GPIOE, GPIOE, GPIOE, GPIOE, GPIOE, GPIOE, GPIOE, GPIOE,
		GPIOD, GPIOD, GPIOD,
		GPIOB, GPIOB, GPIOC, GPIOE, GPIOE, GPIOF, GPIOG, GPIOG, GPIOG, GPIOG,
		0
};

static uint8_t const PINInitTable[] =
{
		0, 1, 2, 3, 4, 5, 12, 13, 14, 15, 0, 1,
		14, 15, 0, 1, 7, 8, 9, 10, 11, 12, 13, 14, 15,
		8, 9, 10,
		5, 6, 0, 0, 1, 11, 4, 5, 8, 15,
		0
};

void SDRAM_Init(void)
{
	volatile uint32_t i;

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN | RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOGEN;

	for(  i=0; GPIOInitTable[i]!=0; i++ )
	{
		gpioPinConfiguration(GPIOInitTable[i], PINInitTable[i],  GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
		gpioPinAlternateFunctionConfiguration(GPIOInitTable[i], PINInitTable[i], 12);
	}
	
	RCC->AHB3ENR |= RCC_AHB3ENR_FMCEN;	
	// Initialization step 1
	FMC_Bank5_6->SDCR[0] = FMC_SDCR1_SDCLK_1  | FMC_SDCR1_RBURST | FMC_SDCR1_RPIPE_1;
	FMC_Bank5_6->SDCR[1] = FMC_SDCR1_NR_0	  | FMC_SDCR1_MWID_0 | FMC_SDCR1_NB | FMC_SDCR1_CAS;
	// Initialization step 2
	FMC_Bank5_6->SDTR[0] = TRC(7)  | TRP(2);
	FMC_Bank5_6->SDTR[1] = TMRD(2) | TXSR(7) | TRAS(4) | TWR(2) | TRCD(2);
	// Initialization step 3	
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	FMC_Bank5_6->SDCMR 	 = 1 | FMC_SDCMR_CTB2 | (1 << 5);
	// Initialization step 4
	for( i=0; i<1000000; i++ );
	// Initialization step 5
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	FMC_Bank5_6->SDCMR 	 = 2 | FMC_SDCMR_CTB2 | (1 << 5);
	// Initialization step 6
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	FMC_Bank5_6->SDCMR 	 = 3 | FMC_SDCMR_CTB2 | (4 << 5);	
	// Initialization step 7
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	FMC_Bank5_6->SDCMR 	 = 4 | FMC_SDCMR_CTB2 | (1 << 5) | (0x231 << 9);
	// Initialization step 8
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	FMC_Bank5_6->SDRTR |= (683 << 1);
	while(FMC_Bank5_6->SDSR & FMC_SDSR_BUSY);
	// Clear SDRAM
	//volatile uint32_t ptr = 0;
	//for(ptr = SDRAM_BASE; ptr < SDRAM_END; ptr += 4)	*((uint32_t *)ptr) = 0xFFFFFFFF;
}
