/*
 * button_border_bitmap.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_BORDER_BITMAP_H_
#define BUTTON_BORDER_BITMAP_H_


#include "button_border.h"
#include "button_bitmap.h"

template<typename Color, typename Pos>
class ButtonBorderBitmap : virtual public ButtonBorder<Color,Pos>, virtual public ButtonBitmap<Color,Pos>
{
public:
	ButtonBorderBitmap( Pos x, Pos y, Color borderColor, const Bitmap<Color> &bitmap,  bool state = true );
	virtual ~ButtonBorderBitmap() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonBorderBitmap<Color,Pos>::ButtonBorderBitmap( Pos x, Pos y, Color borderColor, const Bitmap<Color> &bitmap,  bool state )
:	Button<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, state ),
	ButtonBorder<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, borderColor, state ),
	ButtonBitmap<Color,Pos>( x, y, bitmap, state )
{

}

template<typename Color, typename Pos>
void ButtonBorderBitmap<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBitmap<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonBorder<Color, Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_BORDER_BITMAP_H_ */
