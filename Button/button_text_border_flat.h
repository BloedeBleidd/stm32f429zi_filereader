/*
 * button_text_border_flat.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_BORDER_FLAT_H_
#define BUTTON_TEXT_BORDER_FLAT_H_


#include "button_text.h"
#include "button_border_flat.h"

template<typename Color, typename Pos>
class ButtonTextBorderFlat : virtual public ButtonText<Color,Pos>, virtual public ButtonBorderFlat<Color,Pos>
{
public:
	ButtonTextBorderFlat( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color backgroundColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode = ButtonTextMode::MIDDLE, bool state = true );
	virtual ~ButtonTextBorderFlat() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextBorderFlat<Color,Pos>::ButtonTextBorderFlat( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color backgroundColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonText<Color,Pos>( x, y, width, height, fontColor, name, font, mode, state ),
	ButtonBorder<Color,Pos>( x, y, width, height, borderColor, state ),
	ButtonFlat<Color,Pos>( x, y, width, height, backgroundColor, state ),
	ButtonBorderFlat<Color,Pos>( x, y, width, height, borderColor, backgroundColor, state )
{

}

template<typename Color, typename Pos>
void ButtonTextBorderFlat<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBorderFlat<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonText<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_TEXT_BORDER_FLAT_H_ */
