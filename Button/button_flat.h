/*
 * button_flat.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_FLAT_H_
#define BUTTON_FLAT_H_


#include "button.h"

template<typename Color, typename Pos>
class ButtonFlat : virtual public Button<Color,Pos>
{
protected:
	Color backgroundColor;

public:
	ButtonFlat( Pos x, Pos y, Pos width, Pos height, Color backgroundColor, bool state = true );
	virtual ~ButtonFlat() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;

	void setBackgroundColor( Color backgroundColor )	{ this->backgroundColor = backgroundColor; }
	Color getBackgroundColor() const					{ return backgroundColor; }
};

template<typename Color, typename Pos>
ButtonFlat<Color,Pos>::ButtonFlat( Pos x, Pos y, Pos width, Pos height, Color backgroundColor, bool state )
:	Button<Color,Pos>( x, y, width, height, state )
{
	this->backgroundColor = backgroundColor;
}

template<typename Color, typename Pos>
void ButtonFlat<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	typedef Button<Color,Pos> B;
	lcd.drawRectangleAndFill( B::x+xOffset, B::y+yOffset, B::x+B::width+xOffset, B::y+B::height+yOffset, backgroundColor );
}


#endif /* BUTTON_FLAT_H_ */
