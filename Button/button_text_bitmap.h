/*
 * button_text_bitmap.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_BITMAP_H_
#define BUTTON_TEXT_BITMAP_H_


#include "button_text.h"
#include "button_bitmap.h"

template<typename Color, typename Pos>
class ButtonTextBitmap : virtual public ButtonText<Color,Pos>, virtual public ButtonBitmap<Color,Pos>
{
public:
	ButtonTextBitmap( Pos x, Pos y, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, const Bitmap<Color> &bitmap, bool state = true );
	virtual ~ButtonTextBitmap() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextBitmap<Color,Pos>::ButtonTextBitmap( Pos x, Pos y, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, const Bitmap<Color> &bitmap, bool state )
:	Button<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, state ),
	ButtonText<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, fontColor, name, font, mode, state),
	ButtonBitmap<Color,Pos>( x, y, bitmap, state )
{

}

template<typename Color, typename Pos>
void ButtonTextBitmap<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBitmap<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonText<Color, Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_TEXT_BITMAP_H_ */
