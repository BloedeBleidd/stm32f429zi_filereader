/*
 * button_text.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_H_
#define BUTTON_TEXT_H_


#include "button.h"
#include "font.h"
#include <string>

enum ButtonTextMode { LEFT, MIDDLE, RIGHT };

template<typename Color, typename Pos>
class ButtonText : virtual public Button<Color,Pos>
{
protected:
	std::string name;
	Font::Data font;
	Color fontColor;
	ButtonTextMode mode;

public:
	ButtonText( Pos x, Pos y, Pos width, Pos height, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode = MIDDLE, bool state = true );
	virtual ~ButtonText() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const = 0;
};

template<typename Color, typename Pos>
ButtonText<Color,Pos>::ButtonText( Pos x, Pos y, Pos width, Pos height, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, bool state )
:Button<Color,Pos>( x, y, width, height, state )
{
	this->name = name;
	this->font = font;
	this->fontColor = fontColor;
	this->mode = mode;
}

template<typename Color, typename Pos>
void ButtonText<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	Font::Data lastFont = lcd.getFont();
	lcd.setFont( font );

	Pos xTmp = Button<Color,Pos>::x + xOffset;
	Pos yTmp = Button<Color,Pos>::y + yOffset + ( Button<Color,Pos>::height - lcd.getFontHeight() ) / 2 + 1 ;

	if( mode == MIDDLE )
		xTmp += ( Button<Color,Pos>::width - (Pos)name.length() * (Pos)lcd.getFontWidth() ) / 2;
	else if( mode == RIGHT )
		xTmp += Button<Color,Pos>::width - (Pos)name.length() * (Pos)lcd.getFontWidth();

	lcd.drawString( xTmp, yTmp, name, fontColor );
	lcd.setFont( lastFont );
}


#endif /* BUTTON_TEXT_H_ */
