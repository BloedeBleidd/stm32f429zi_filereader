/*
 * button_text_border_transparent.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_BORDER_TRANSPARENT_H_
#define BUTTON_TEXT_BORDER_TRANSPARENT_H_


#include "button_text.h"
#include "button_border_transparent.h"

template<typename Color, typename Pos>
class ButtonTextBorderTransparent : virtual public ButtonText<Color,Pos>, virtual public ButtonBorderTransparent<Color,Pos>
{
public:
	ButtonTextBorderTransparent( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode = ButtonTextMode::MIDDLE, bool state = true );
	virtual ~ButtonTextBorderTransparent() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextBorderTransparent<Color,Pos>::ButtonTextBorderTransparent( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonText<Color,Pos>( x, y, width, height, fontColor, name, font, mode, state ),
	ButtonBorder<Color,Pos>( x, y, width, height, borderColor, state ),
	ButtonBorderTransparent<Color,Pos>( x, y, width, height, borderColor, state )
{

}

template<typename Color, typename Pos>
void ButtonTextBorderTransparent<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBorderTransparent<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonText<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_TEXT_BORDER_TRANSPARENT_H_ */
