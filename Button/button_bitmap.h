/*
 * button_bitmap.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_BITMAP_H_
#define BUTTON_BITMAP_H_


#include "button.h"
#include "bitmap.h"

template<typename Color, typename Pos>
class ButtonBitmap : virtual public Button<Color,Pos>
{
protected:
	Bitmap<Color> *bitmap;

public:
	ButtonBitmap( Pos x, Pos y, const Bitmap<Color> &bitmap, bool state = true );
	virtual ~ButtonBitmap() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonBitmap<Color,Pos>::ButtonBitmap( Pos x, Pos y, const Bitmap<Color> &bitmap, bool state )
:	Button<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, state )
{
	this->bitmap = &bitmap;
}

template<typename Color, typename Pos>
void ButtonBitmap<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	lcd.drawBitmap( Button<Color,Pos>::x+xOffset, Button<Color,Pos>::y+yOffset, bitmap );
}


#endif /* BUTTON_BITMAP_H_ */
