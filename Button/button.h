/*
 * button.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "graphic.h"

template<typename Color, typename Pos>
class Button
{
protected:
	Pos x, y;
	Pos width, height;
	bool state;

public:
	Button( Pos x, Pos y, Pos width, Pos height, bool state );
	virtual ~Button() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset, Pos yOffset ) const = 0;

	bool isPressed( Pos x, Pos y ) const;
	bool isEnabled() const						{ return state; }

	void enable()								{ state = true; }
	void disable()								{ state = false; }

	void setPosition( Pos x, Pos y )			{ this->x = x; this->y = y; }
	void setDimensions( Pos width, Pos height )	{ this->width = width; this->height = height; }
	void setState( bool state )					{ this->state = state; }

	Pos getX() const							{ return x; }
	Pos getY() const							{ return y; }
	Pos getWidth() const						{ return width; }
	Pos getHeight() const						{ return height; }
};

template<typename Color, typename Pos>
Button<Color,Pos>::Button( Pos x, Pos y, Pos width, Pos height, bool state )
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->state = state;
}

template<typename Color, typename Pos>
bool Button<Color,Pos>::isPressed( Pos x, Pos y ) const
{
	if( state == true && ( ( x >= this->x ) && ( x <= this->x+this->width ) && ( y >= this->y ) && ( y <= this->y+this->height ) ) )
		return true;

	return false;
}


#endif /* BUTTON_H_ */
