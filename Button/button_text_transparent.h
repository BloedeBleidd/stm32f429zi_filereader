/*
 * button_text_transparent.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_TRANSPARENT_H_
#define BUTTON_TEXT_TRANSPARENT_H_


#include "button_text.h"
#include "button_transparent.h"

template<typename Color, typename Pos>
class ButtonTextTransparent : virtual public ButtonText<Color,Pos>, virtual public ButtonTransparent<Color,Pos>
{
public:
	ButtonTextTransparent( Pos x, Pos y, Pos width, Pos height, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode = ButtonTextMode::MIDDLE, bool state = true );
	virtual ~ButtonTextTransparent() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextTransparent<Color,Pos>::ButtonTextTransparent( Pos x, Pos y, Pos width, Pos height, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonText<Color,Pos>( x, y, width, height, fontColor, name, font, mode, state ),
	ButtonTransparent<Color,Pos>( x, y, width, height, state )
{

}

template<typename Color, typename Pos>
void ButtonTextTransparent<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonText<Color,Pos>::draw( lcd, xOffset, yOffset );
}



#endif /* BUTTON_TEXT_TRANSPARENT_H_ */
