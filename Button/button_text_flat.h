/*
 * button_text_flat.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_FLAT_H_
#define BUTTON_TEXT_FLAT_H_


#include "button_text.h"
#include "button_flat.h"

template<typename Color, typename Pos>
class ButtonTextFlat : virtual public ButtonText<Color,Pos>, virtual public ButtonFlat<Color,Pos>
{
public:
	ButtonTextFlat( Pos x, Pos y, Pos width, Pos height, Color backgroundColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode = MIDDLE, bool state = true );
	virtual ~ButtonTextFlat() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextFlat<Color,Pos>::ButtonTextFlat( Pos x, Pos y, Pos width, Pos height, Color backgroundColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonText<Color,Pos>( x, y, width, height, fontColor, name, font, mode, state ),
	ButtonFlat<Color,Pos>( x, y, width, height, backgroundColor, state )
{

}

template<typename Color, typename Pos>
void ButtonTextFlat<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonFlat<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonText<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_TEXT_FLAT_H_ */
