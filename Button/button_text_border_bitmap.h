/*
 * button_text_border_bitmap.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TEXT_BORDER_BITMAP_H_
#define BUTTON_TEXT_BORDER_BITMAP_H_


#include "button_text.h"
#include "button_border_bitmap.h"

template<typename Color, typename Pos>
class ButtonTextBorderBitmap : virtual public ButtonText<Color,Pos>, virtual public ButtonBorderBitmap<Color,Pos>
{
public:
	ButtonTextBorderBitmap( Pos x, Pos y, Color borderColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, const Bitmap<Color> &bitmap, bool state = true );
	virtual ~ButtonTextBorderBitmap() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTextBorderBitmap<Color,Pos>::ButtonTextBorderBitmap( Pos x, Pos y, Color borderColor, Color fontColor, const char *name, const Font::Data &font, ButtonTextMode mode, const Bitmap<Color> &bitmap, bool state )
:	Button<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, state ),
	ButtonText<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, fontColor, name, font, mode, state ),
	ButtonBorder<Color,Pos>( x, y, (Pos)bitmap.width, (Pos)bitmap.height, borderColor, state ),
	ButtonBitmap<Color,Pos>( x, y, bitmap, state ),
	ButtonBorderBitmap<Color,Pos>( x, y, borderColor, bitmap, state )
{

}

template<typename Color, typename Pos>
void ButtonTextBorderBitmap<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBorderBitmap<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonText<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_TEXT_BORDER_BITMAP_H_ */
