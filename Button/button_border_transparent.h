/*
 * button_border_transparent.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_BORDER_TRANSPARENT_H_
#define BUTTON_BORDER_TRANSPARENT_H_


#include "button_border.h"

template<typename Color, typename Pos>
class ButtonBorderTransparent : virtual public ButtonBorder<Color,Pos>
{
public:
	ButtonBorderTransparent( Pos x, Pos y, Pos width, Pos height, Color borderColor, bool state = true );
	virtual ~ButtonBorderTransparent() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonBorderTransparent<Color,Pos>::ButtonBorderTransparent( Pos x, Pos y, Pos width, Pos height, Color borderColor, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonBorder<Color,Pos>( x, y, width, height, borderColor, state )
{

}

template<typename Color, typename Pos>
void ButtonBorderTransparent<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonBorder<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_BORDER_TRANSPARENT_H_ */
