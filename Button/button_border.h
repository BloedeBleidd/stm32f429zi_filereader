/*
 * button_border.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_BORDER_H_
#define BUTTON_BORDER_H_


#include "button.h"

template<typename Color, typename Pos>
class ButtonBorder : virtual public Button<Color,Pos>
{
protected:
	Color borderColor;

public:
	ButtonBorder( Pos x, Pos y, Pos width, Pos height, Color borderColor, bool state = true );
	virtual ~ButtonBorder() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const = 0;

	void setBorder( Color borderColor )			{ this->borderColor = borderColor; }
	Color getBorderColor() const				{ return borderColor; }
};

template<typename Color, typename Pos>
ButtonBorder<Color,Pos>::ButtonBorder( Pos x, Pos y, Pos width, Pos height, Color borderColor, bool state )
: Button<Color,Pos>( x, y, width, height, borderColor, state )
{
	this->borderColor = borderColor;
}

template<typename Color, typename Pos>
void ButtonBorder<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	typedef Button<Color,Pos> B;
	lcd.drawRectangle( B::x+xOffset, B::y+yOffset, B::x+B::width+xOffset, B::y+B::height+yOffset, borderColor );
}


#endif /* BUTTON_BORDER_H_ */
