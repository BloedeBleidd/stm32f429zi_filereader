/*
 * button_border_flat.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_BORDER_FLAT_H_
#define BUTTON_BORDER_FLAT_H_


#include "button_border.h"
#include "button_flat.h"

template<typename Color, typename Pos>
class ButtonBorderFlat : virtual public ButtonBorder<Color,Pos>, virtual public ButtonFlat<Color,Pos>
{
public:
	ButtonBorderFlat( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color backgroundColor, bool state = true );
	virtual ~ButtonBorderFlat() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonBorderFlat<Color,Pos>::ButtonBorderFlat( Pos x, Pos y, Pos width, Pos height, Color borderColor, Color backgroundColor, bool state )
:	Button<Color,Pos>( x, y, width, height, state ),
	ButtonBorder<Color,Pos>( x, y, width, height, borderColor, state ),
	ButtonFlat<Color,Pos>( x, y, width, height, backgroundColor, state )
{

}

template<typename Color, typename Pos>
void ButtonBorderFlat<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{
	ButtonFlat<Color,Pos>::draw( lcd, xOffset, yOffset );
	ButtonBorder<Color,Pos>::draw( lcd, xOffset, yOffset );
}


#endif /* BUTTON_BORDER_FLAT_H_ */
