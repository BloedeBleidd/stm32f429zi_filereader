/*
 * button_transparent.h
 *
 *  Created on: Sep 13, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BUTTON_TRANSPARENT_H_
#define BUTTON_TRANSPARENT_H_


#include "button.h"

template<typename Color, typename Pos>
class ButtonTransparent : virtual public Button<Color,Pos>
{
public:
	ButtonTransparent( Pos x, Pos y, Pos width, Pos height, bool state = true );
	virtual ~ButtonTransparent() {}

	virtual void draw( Graphic<Color,Pos> &lcd, Pos xOffset = 0, Pos yOffset = 0 ) const;
};

template<typename Color, typename Pos>
ButtonTransparent<Color,Pos>::ButtonTransparent( Pos x, Pos y, Pos width, Pos height, bool state )
:	Button<Color,Pos>( x, y, width, height, state )
{

}

template<typename Color, typename Pos>
void ButtonTransparent<Color,Pos>::draw( Graphic<Color,Pos> &lcd, Pos xOffset , Pos yOffset ) const
{

}


#endif /* BUTTON_TRANSPARENT_H_ */
