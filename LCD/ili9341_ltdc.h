/*
 * ili9341_ltdc.h
 *
 *  Created on: Sep 8, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef ILI9341_LTDC_H_
#define ILI9341_LTDC_H_

#include "ili9341_spi.h"

class Ili9341_LTDC : public Ili9341_Spi<uint16_t,int32_t>
{
public:
	enum Layer
	{
		BOTTOM	= 0,
		TOP		= 1
	};

private:
	static const uint32_t ILI9341_HSYNC	= 9;	/* Horizontal synchronization */
	static const uint32_t ILI9341_HBP	= 29;	/* Horizontal back porch      */
	static const uint32_t ILI9341_HFP	= 2;	/* Horizontal front porch     */
	static const uint32_t ILI9341_VSYNC	= 1;	/* Vertical synchronization   */
	static const uint32_t ILI9341_VBP	= 3;	/* Vertical back porch        */
	static const uint32_t ILI9341_VFP	= 2;	/* Vertical front porch       */

	static const uint32_t ACTIVE_W		= 269;
	static const uint32_t ACTIVE_H		= 323;

	static const uint32_t TOTAL_WIDTH	= (ILI9341_HSYNC + ILI9341_HBP + Ili9341::WIDTH_MAX + ILI9341_HFP - 1);
	static const uint32_t TOTAL_HEIGHT	= (ILI9341_VSYNC + ILI9341_VBP + Ili9341::HEIGHT_MAX + ILI9341_VFP - 1 + 2);

	const static GPIO_TypeDef * GPIOInitTable[23];
	const static uint8_t PINInitTable[22];
	const static uint8_t AFInitTable[22];
	const static Ili9341_Spi::SpiInitStruct spi;

	Layer layerCurrent = BOTTOM;
	uint16_t *VRAM1 = nullptr;
	uint16_t *VRAM2 = nullptr;

	bool isLayerTopUsed()			{ return VRAM2==nullptr?false:true; }
	void reloadImmediate()			{ LTDC->SRCR = LTDC_SRCR_IMR; }
	void reloadVerticalBlanking()	{ LTDC->SRCR = LTDC_SRCR_VBR; }
	void hardwareInitialize();
	void Ili9341Initialize();
	void buffCopy32Bit( void *dst, void const *src, uint32_t len );
	void refresh();
public:
	Ili9341_LTDC( Orientation orientation = Landscape_1, bool use2LayerBuffer = true, void *graphicBufferAddr = nullptr, void *VRAM1Addr = nullptr, void *VRAM2Addr = nullptr );
	~Ili9341_LTDC();

	void backgroundColorSet(uint32_t color) { LTDC->BCCR = color; } // RGB888 (reserved,red,green,blue)

	bool layerSet(Layer);
	bool layerChange();

	bool layerOpacitySetTop(uint8_t);
	void layerOpacitySetBottom(uint8_t);
	void layerOpacitySetCurrent(uint8_t);
	bool layerOpacitySetNotCurrent(uint8_t);

	bool layerOpacityGetTop(uint8_t &);
	void layerOpacityGetBottom(uint8_t &);
	void layerOpacityGetCurrent(uint8_t &);
	bool layerOpacityGetNotCurrent(uint8_t &);

	bool layerCopyFromTopToBottom();
	bool layerCopyFromBottomToTop();
	bool layerCopyFromCurrent();
	bool layerCopyToCurrent();

	bool layerOn(Layer);
	bool layerOnTop();
	void layerOnBottom();
	void layerOnCurrent();
	bool layerOnNotCurrent();

	bool layerOff(Layer);
	bool layerOffTop();
	void layerOffBottom();
	void layerOffCurrent();
	bool layerOffNotCurrent();

	bool refreshLayer(Layer);
	bool refreshTop();
	void refreshBottom();
	void refreshCurrentLayer();
	bool refreshNotCurrentLayer();

	void displayOn();
	void displayOff();
};

#endif /* ILI9341_LTDC_H_ */
