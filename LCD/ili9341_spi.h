/*
 * ili9341_spi.h
 *
 *  Created on: Sep 7, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef Ili9341_SPI_H_
#define Ili9341_SPI_H_

#include "gpio.h"
#include "RTOS_includes.h"
#include "stm32f4xx.h"
#include "ili9341.h"
#include "graphic.h"

template<typename Color, typename Pos>
class Ili9341_Spi : public Ili9341, public Graphic<Color,Pos>
{
public:
	struct SpiInitStruct
	{
		 SPI_TypeDef	*spi;
		 uint8_t		altFunction;
		 GPIO_TypeDef	*gpioChipSelect;
		 PinNumber		chipSelect;
		 GPIO_TypeDef	*gpioDataCommand;
		 PinNumber		dataCommand;
		 GPIO_TypeDef	*gpioMosi;
		 PinNumber		mosi;
		 GPIO_TypeDef	*gpioClock;
		 PinNumber		clock;
	};
protected:
	enum CsControl
	{
		SELECT		= 0,
		DESELECT	= 1,
	};

	enum DcControl
	{
		DATA		= 0,
		COMMAND		= 1,
	};

	SpiInitStruct spiStruct;

	void delayMs( TickType_t );
	void csControl( CsControl );
	void dcControl( DcControl );
	void spiFastMode();
	void spiSlowMode();
	void spiSetMode8Bit();
	void spiSetMode16Bit();
	uint16_t spiExchangeData( uint16_t );
	void spiWriteByte( Ili9341RegisterSize );
	void spiWriteCmd( Ili9341RegisterSize );
	void spiWriteData( Ili9341RegisterSize );
	void spiSetWindow( uint16_t xStart, uint16_t yStart, uint16_t xStop, uint16_t yStop );
	void spiSendBuffer( uint32_t n );
	void Ili9341Initialize();

	Ili9341_Spi(){};
public:
	Ili9341_Spi( uint16_t width, uint16_t height, const SpiInitStruct &initStruct, Orientation orientation = Landscape_2, void *buffAddress = nullptr );
	~Ili9341_Spi();

	void refresh();
};

template<typename Color, typename Pos>
Ili9341_Spi<Color,Pos>::Ili9341_Spi( uint16_t width, uint16_t height, const SpiInitStruct &initStruct, Orientation orientation, void *buffAddress )
{
	this->width = width;
	this->height = height;
	this->orientation = orientation;
	this->amountOfPixels = width*height;

	spiStruct = initStruct;
	this->createBuffer( buffAddress, this->graphicBuffer );

	// SCK, MOSI, DC, CS
	gpioPinConfiguration( spiStruct.gpioMosi, spiStruct.mosi, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED );
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioMosi, spiStruct.mosi, spiStruct.altFunction );
	gpioPinConfiguration( spiStruct.gpioClock, spiStruct.clock, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED );
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioClock, spiStruct.clock, spiStruct.altFunction );
	gpioPinConfiguration( spiStruct.gpioChipSelect, spiStruct.chipSelect, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED );
	gpioPinConfiguration( spiStruct.gpioDataCommand, spiStruct.dataCommand, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED );

	// init spi
	spiStruct.spi->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	spiSlowMode();
	spiSetMode8Bit();

	Ili9341_Spi::Ili9341Initialize();
}

template<typename Color, typename Pos>
Ili9341_Spi<Color,Pos>::~Ili9341_Spi()
{
	this->deleteBuffer( this->graphicBuffer );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::csControl( CsControl state )
{
	if( state == SELECT )
		gpioBitReset( spiStruct.gpioChipSelect, spiStruct.chipSelect );
	else
	{
		while( spiStruct.spi->SR & SPI_SR_BSY );	//WaitUntilSpiIsBusy
		gpioBitSet( spiStruct.gpioChipSelect, spiStruct.chipSelect );
	}
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::dcControl( DcControl state )
{
	if( state == COMMAND )
		gpioBitReset( spiStruct.gpioDataCommand, spiStruct.dataCommand );
	else
		gpioBitSet( spiStruct.gpioDataCommand, spiStruct.dataCommand );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiFastMode()
{
	spiStruct.spi->CR1 &= ~SPI_CR1_SPE; // disable
	spiStruct.spi->CR1 = (spiStruct.spi->CR1 & ~SPI_CR1_BR_Msk) | 0; //div2
	spiStruct.spi->CR1 |= SPI_CR1_SPE;// enable
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiSlowMode()
{
	spiStruct.spi->CR1 &= ~SPI_CR1_SPE; // disable
	spiStruct.spi->CR1 = (spiStruct.spi->CR1 & ~SPI_CR1_BR_Msk) | SPI_CR1_BR; //div256
	spiStruct.spi->CR1 |= SPI_CR1_SPE;// enable
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiSetMode8Bit()
{
	spiStruct.spi->CR1 &= ~SPI_CR1_SPE; // disable
	spiStruct.spi->CR1 &= ~SPI_CR1_DFF;
	spiStruct.spi->CR1 |= SPI_CR1_SPE;// enable
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiSetMode16Bit()
{
	spiStruct.spi->CR1 &= ~SPI_CR1_SPE; // disable
	spiStruct.spi->CR1 |= SPI_CR1_DFF;
	spiStruct.spi->CR1 |= SPI_CR1_SPE;// enable
}

template<typename Color, typename Pos>
uint16_t Ili9341_Spi<Color,Pos>::spiExchangeData( uint16_t data )
{
	while( !(spiStruct.spi->SR & SPI_SR_TXE) ); //WaitUntilSpiTransmits
	spiStruct.spi->DR = data;
	while( !(spiStruct.spi->SR & SPI_SR_RXNE) ); //WaitUntilSpiReceive
	data = spiStruct.spi->DR;
	return data;
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::delayMs( TickType_t del )
{
	vTaskDelay( pdMS_TO_TICKS( del ) );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiWriteByte( Ili9341RegisterSize byte )
{
	csControl( SELECT );
	spiSetMode8Bit();
	uint8_t __attribute__((unused)) dummy = spiExchangeData( byte );
	csControl( DESELECT );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiWriteCmd( Ili9341RegisterSize cmd )
{
	dcControl( COMMAND );
	spiWriteByte( cmd );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiWriteData( Ili9341RegisterSize data )
{
	dcControl( DATA );
	spiWriteByte( data );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::Ili9341Initialize()
{
	//Start initial Sequence
	spiWriteCmd( SOFT_RES ); //software reset
	delayMs(100);
	spiWriteCmd( DISPLAY_OFF ); // display off

	spiWriteCmd( POWERA );
	spiWriteData(0x39);
	spiWriteData(0x2C);
	spiWriteData(0x00);
	spiWriteData(0x34);
	spiWriteData(0x02);
	spiWriteCmd( POWERB );
	spiWriteData(0x00);
	spiWriteData(0xC1);
	spiWriteData(0x30);
	spiWriteCmd( DTCA );
	spiWriteData(0x85);
	spiWriteData(0x00);
	spiWriteData(0x78);
	spiWriteCmd( DTCB );
	spiWriteData(0x00);
	spiWriteData(0x00);
	spiWriteCmd( POWER_SEQ );
	spiWriteData(0x64);
	spiWriteData(0x03);
	spiWriteData(0x12);
	spiWriteData(0x81);
	spiWriteCmd( PRC );
	spiWriteData(0x20);
	spiWriteCmd( POWER1 );
	spiWriteData(0x23);
	spiWriteCmd( POWER2 );
	spiWriteData(0x10);
	spiWriteCmd( VCOM1 );
	spiWriteData(0x3E);
	spiWriteData(0x28);
	spiWriteCmd( VCOM2 );
	spiWriteData(0x86);
	spiWriteCmd( MAC );
	spiWriteData(0b11101000); // MY MX MV ML BGR MH 0 0
	spiWriteCmd( PIXEL_FORMAT );
	spiWriteData(0x55);
	spiWriteCmd( FRAME_CONT_NORM );
	spiWriteData(0x00);
	spiWriteData(0x18);
	spiWriteCmd( DFC );
	spiWriteData(0x08);
	spiWriteData(0x82);
	spiWriteData(0x27);
	spiWriteCmd( GAMMA3_EN );
	spiWriteData(0x00);
	spiWriteCmd( GAMMA );
	spiWriteData(0x01);
	spiWriteCmd( PGAMMA );
	spiWriteData(0x0F);
	spiWriteData(0x31);
	spiWriteData(0x2B);
	spiWriteData(0x0C);
	spiWriteData(0x0E);
	spiWriteData(0x08);
	spiWriteData(0x4E);
	spiWriteData(0xF1);
	spiWriteData(0x37);
	spiWriteData(0x07);
	spiWriteData(0x10);
	spiWriteData(0x03);
	spiWriteData(0x0E);
	spiWriteData(0x09);
	spiWriteData(0x00);
	spiWriteCmd( NGAMMA );
	spiWriteData(0x00);
	spiWriteData(0x0E);
	spiWriteData(0x14);
	spiWriteData(0x03);
	spiWriteData(0x11);
	spiWriteData(0x07);
	spiWriteData(0x31);
	spiWriteData(0xC1);
	spiWriteData(0x48);
	spiWriteData(0x08);
	spiWriteData(0x0F);
	spiWriteData(0x0C);
	spiWriteData(0x31);
	spiWriteData(0x36);
	spiWriteData(0x0F);

	spiWriteCmd( SLEEP_OUT );
	delayMs(100);
	spiWriteCmd( DISPLAY_ON ); // display on
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiSetWindow( uint16_t xStart, uint16_t yStart, uint16_t xStop, uint16_t yStop )
{
	spiWriteCmd( COLUMN_ADDR );
	spiWriteData( xStart >> 8 );
	spiWriteData( xStart );
	spiWriteData( xStop >> 8 );
	spiWriteData( xStop );
	spiWriteCmd( PAGE_ADDR );
	spiWriteData( yStart >> 8 );
	spiWriteData( yStart );
	spiWriteData( yStop >> 8 );
	spiWriteData( yStop );
	spiWriteCmd( GRAM );
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::spiSendBuffer( uint32_t n )
{
	csControl( SELECT );
	dcControl( DATA );
	spiSetMode16Bit();
	spiFastMode();

	uint16_t __attribute__((unused)) dummy;
	Color *buf = this->graphicBuffer;

	for( ; n; n-- )
		dummy = spiExchangeData( *buf++ );

	csControl( DESELECT );
	spiSlowMode();
	spiSetMode8Bit();
}

template<typename Color, typename Pos>
void Ili9341_Spi<Color,Pos>::refresh( )
{
	if( this->orientation == Landscape_1 || this->orientation == Landscape_2 )
		spiSetWindow( 0, 0, this->width-1, this->height-1 );
	else
		spiSetWindow( 0, 0, this->height-1, this->width-1 );

	delayMs( 1 );
	spiSendBuffer( this->amountOfPixels );
}

#endif /* Ili9341_SPI_H_ */
