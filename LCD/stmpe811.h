/*
 * stmpe811.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef STMPE811_H_
#define STMPE811_H_

#include "stm32f4xx.h"
#include "gpio.h"
#include "touch_panel.h"
#include "RTOS_includes.h"

/*
STMPE811	STM32F429	Description
SCL			PA8			Clock pin for I2C
SDA			PC9			Data pin for I2C
INT		    PA15		Int pin for STMPE811
			I2C3
			AF4
*/

const uint16_t STMPE911_MAX_RES = 4096;
// STMPE811 Chip ID on reset
const uint16_t STMPE811_CHIP_ID_VALUE =	0x0811;	//Chip ID
// I2C address
const uint8_t STMPE811_ADDRESS =		0x82;
// Registers
const uint8_t STMPE811_CHIP_ID =		0x00;	//STMPE811 Device identification
const uint8_t STMPE811_ID_VER =			0x02;	//STMPE811 Revision number; 0x01 for engineering sample; 0x03 for final silicon
const uint8_t STMPE811_SYS_CTRL1 =		0x03;	//Reset control
const uint8_t STMPE811_SYS_CTRL2 =		0x04;	//Clock control
const uint8_t STMPE811_SPI_CFG =		0x08;	//SPI interface configuration
const uint8_t STMPE811_INT_CTRL =		0x09;	//Interrupt control register
const uint8_t STMPE811_INT_EN =			0x0A;	//Interrupt enable register
const uint8_t STMPE811_INT_STA =		0x0B;	//Interrupt status register
const uint8_t STMPE811_GPIO_EN =		0x0C;	//GPIO interrupt enable register
const uint8_t STMPE811_GPIO_INT_STA =	0x0D;	//GPIO interrupt status register
const uint8_t STMPE811_ADC_INT_EN =		0x0E;	//ADC interrupt enable register
const uint8_t STMPE811_ADC_INT_STA =	0x0F;	//ADC interface status register
const uint8_t STMPE811_GPIO_SET_PIN =	0x10;	//GPIO set pin register
const uint8_t STMPE811_GPIO_CLR_PIN =	0x11;	//GPIO clear pin register
const uint8_t STMPE811_MP_STA =			0x12;	//GPIO monitor pin state register
const uint8_t STMPE811_GPIO_DIR =		0x13;	//GPIO direction register
const uint8_t STMPE811_GPIO_ED =		0x14;	//GPIO edge detect register
const uint8_t STMPE811_GPIO_RE =		0x15;	//GPIO rising edge register
const uint8_t STMPE811_GPIO_FE =		0x16;	//GPIO falling edge register
const uint8_t STMPE811_GPIO_AF =		0x17;	//alternate function register
const uint8_t STMPE811_ADC_CTRL1 =		0x20;	//ADC control
const uint8_t STMPE811_ADC_CTRL2 =		0x21;	//ADC control
const uint8_t STMPE811_ADC_CAPT =		0x22;	//To initiate ADC data acquisition
const uint8_t STMPE811_ADC_DATA_CHO =	0x30;	//ADC channel 0
const uint8_t STMPE811_ADC_DATA_CH1 =	0x32;	//ADC channel 1
const uint8_t STMPE811_ADC_DATA_CH2 =	0x34;	//ADC channel 2
const uint8_t STMPE811_ADC_DATA_CH3 =	0x36;	//ADC channel 3
const uint8_t STMPE811_ADC_DATA_CH4 =	0x38;	//ADC channel 4
const uint8_t STMPE811_ADC_DATA_CH5 =	0x3A;	//ADC channel 5
const uint8_t STMPE811_ADC_DATA_CH6 =	0x3C;	//ADC channel 6
const uint8_t STMPE811_ADC_DATA_CH7 =	0x3E;	//ADC channel 7
const uint8_t STMPE811_TSC_CTRL =		0x40;	//4-wire touchscreen controller setup
const uint8_t STMPE811_TSC_CFG =		0x41;	//Touchscreen controller configuration
const uint8_t STMPE811_WDW_TR_X =		0x42;	//Window setup for top right X
const uint8_t STMPE811_WDW_TR_Y =		0x44;	//Window setup for top right Y
const uint8_t STMPE811_WDW_BL_X =		0x46;	//Window setup for bottom left X
const uint8_t STMPE811_WDW_BL_Y =		0x48;	//Window setup for bottom left Y
const uint8_t STMPE811_FIFO_TH =		0x4A;	//FIFO level to generate interrupt
const uint8_t STMPE811_FIFO_STA =		0x4B;	//Current status of FIFO
const uint8_t STMPE811_FIFO_SIZE =		0x4C;	//Current filled level of FIFO
const uint8_t STMPE811_TSC_DATA_X =		0x4D;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_Y =		0x4F;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_Z =		0x51;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_XYZ =	0x52;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_FRACTION_Z =	0x56;	//Touchscreen controller FRACTION_Z
const uint8_t STMPE811_TSC_DATA =		0x57;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_I_DRIVE =	0x58;	//Touchscreen controller drivel
const uint8_t STMPE811_TSC_SHIELD =		0x59;	//Touchscreen controller shield
const uint8_t STMPE811_TEMP_CTRL =		0x60;	//Temperature sensor setup
const uint8_t STMPE811_TEMP_DATA =		0x61;	//Temperature data access port
const uint8_t STMPE811_TEMP_TH =		0x62;	//Threshold for temperature controlled interrupt

template<typename T>
class Stmpe811 : public TouchPanel<T>
{
public:
	struct I2CInitStruct
	{
		I2C_TypeDef		*i2c;
		uint8_t			alterfunction;
		GPIO_TypeDef	*gpioSCL;
		PinNumber		SCL;
		GPIO_TypeDef	*gpioSDA;
		PinNumber		SDA;
	};

	Stmpe811( T width, T height, Orientation orientation, const I2CInitStruct &initStruct );
	Stmpe811( T width, T height, T xMin, T xMax, T yMin, T yMax, Orientation orientation, const I2CInitStruct &initStruct );
	bool update(void);

private:
	typedef uint8_t Data;
	typedef uint8_t Length;

	T x,y,z;
	uint32_t dummy = 0;
	I2CInitStruct i2cStruct;

	void delayMs( TickType_t del );
	void i2cInit();
	bool driverInitialize();
	void writeRegister( Data address, Data data );
	void readRegisters( Data address, Data* data, Length length );
	void convertDriverDataToXYZ();
	void resetFIFO();
};

template<typename T>
Stmpe811<T>::Stmpe811( T width, T height, T xMin, T xMax, T yMin, T yMax, Orientation orientation, const I2CInitStruct &initStruct )
{
	i2cStruct = initStruct;
	TouchPanel<T>::width = width;
	TouchPanel<T>::height = height;
	TouchPanel<T>::orientation = orientation;
	TouchPanel<T>::coordinates.x = 0;
	TouchPanel<T>::coordinates.y = 0;
	TouchPanel<T>::coordinates.z = 0;
	TouchPanel<T>::zMin = 0;
	TouchPanel<T>::pressState = false;
	TouchPanel<T>::calibrate( xMin, xMax, yMin, yMax );

	Stmpe811::i2cInit();
	Stmpe811::driverInitialize();
}

template<typename T>
Stmpe811<T>::Stmpe811( T width, T height, Orientation orientation, const I2CInitStruct &initStruct )
{
	Stmpe811( width, height, 0, width, 0, height, orientation, initStruct );
}

template<typename T>
void Stmpe811<T>::delayMs( TickType_t del )
{
	TickType_t lastTime = xTaskGetTickCount();
	vTaskDelayUntil( &lastTime, pdMS_TO_TICKS( del ) );
}

template<typename T>
void Stmpe811<T>::writeRegister( Data address, Data data )
{
	i2cStruct.i2c->CR1 |= I2C_CR1_START; 		// request a start
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_SB ));	// wait for start to finish // read of SR1 clears the flag
	i2cStruct.i2c->DR = STMPE811_ADDRESS;// transfer address (Transmit)
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_ADDR));			// wait for address transfer
	dummy = i2cStruct.i2c->SR2;					// clear the flag
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_TXE));				// wait for DR empty
	i2cStruct.i2c->DR = address;
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_TXE));				// wait for DR empty
	i2cStruct.i2c->DR = data;			// transfer one byte
	while((!(i2cStruct.i2c->SR1 & I2C_SR1_TXE)) || (!(i2cStruct.i2c->SR1 & I2C_SR1_BTF)));			// wait for bus not-busy
	i2cStruct.i2c->CR1 |= I2C_CR1_STOP;				// request a stop
}

template<typename T>
void Stmpe811<T>::readRegisters( Data address, Data* data, Length length )
{
	i2cStruct.i2c->CR1 |= I2C_CR1_ACK;
	i2cStruct.i2c->CR1 |= I2C_CR1_START; 		// request a start
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_SB ));	// wait for start to finish // read of SR1 clears the flag
	i2cStruct.i2c->DR = STMPE811_ADDRESS;		// transfer address (Transmit)
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_ADDR));					// wait for address transfer
	dummy = i2cStruct.i2c->SR2;							// clear the flag
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_TXE));						// wait for DR empty
	i2cStruct.i2c->DR = address;
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_BTF));					// wait for bus not-busy
	i2cStruct.i2c->CR1 |= I2C_CR1_START; // request a start
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_SB ));	// wait for start to finish // read of SR1 clears the flag
	i2cStruct.i2c->DR = STMPE811_ADDRESS|0x01;	// transfer address (Receive)
	while(!(i2cStruct.i2c->SR1 & I2C_SR1_ADDR));					// wait for address transfer
	dummy = i2cStruct.i2c->SR2;							// clear the flag

	while(length)									// transfer whole block
	{
		if(length == 1)
			i2cStruct.i2c->CR1 &= ~I2C_CR1_ACK;
		while(!(i2cStruct.i2c->SR1 & I2C_SR1_RXNE));					// wait for DR fill
		*( data++ ) = i2cStruct.i2c->DR;	// receive one byte, increment pointer
		length--;
	}

	i2cStruct.i2c->CR1 |= I2C_CR1_STOP;				// request a stop
}

template<typename T>
void Stmpe811<T>::i2cInit( void )
{
	gpioPinConfiguration( i2cStruct.gpioSCL, i2cStruct.SCL, GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED_PULL_UP );
	gpioPinAlternateFunctionConfiguration( i2cStruct.gpioSCL, i2cStruct.SCL, i2cStruct.alterfunction );
	gpioPinConfiguration( i2cStruct.gpioSDA, i2cStruct.SDA, GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED_PULL_UP );
	gpioPinAlternateFunctionConfiguration( i2cStruct.gpioSDA, i2cStruct.SDA, i2cStruct.alterfunction );

	const uint32_t CLK = 400000; // 400kHz

	i2cStruct.i2c->CR1 |= I2C_CR1_SWRST;
	i2cStruct.i2c->CR1 &= ~I2C_CR1_SWRST;
	i2cStruct.i2c->CR1 &= ~I2C_CR1_PE;
	while(i2cStruct.i2c->CR1 & I2C_CR1_PE);
	// i2c timings setup
	i2cStruct.i2c->CR2 = SystemCoreClock/4/1000000;					//frequency
	i2cStruct.i2c->TRISE = SystemCoreClock/4/1000000+1;				// limit slope
	i2cStruct.i2c->CCR = I2C_CCR_FS + SystemCoreClock/3/4/CLK;		// setup speed CLK
	i2cStruct.i2c->CR1 = I2C_CR1_PE;								// enable
}

template<typename T>
bool Stmpe811<T>::driverInitialize()
{
	Data tmp;
	uint8_t Buffer[2];
	int16_t chipID;

	/* Reset */
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x02);		// software reset start
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x00);		// software reset stop
	Stmpe811::delayMs(2);

	/* Check for driver connected */
	Stmpe811::readRegisters(STMPE811_CHIP_ID,Buffer,2);
	chipID = Buffer[0]<<8|Buffer[1];
	if (chipID != STMPE811_CHIP_ID_VALUE)
		return ERROR;

	/* Reset */
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x02);		// software reset start
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x00);		// software reset stop
	Stmpe811::delayMs(2);

	/* Get the current register value */
	Stmpe811::readRegisters(STMPE811_SYS_CTRL2, &tmp, 1);
	tmp &= ~(0x01);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL2, tmp);
	Stmpe811::readRegisters(STMPE811_SYS_CTRL2, &tmp, 1);
	tmp &= ~(0x02);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL2, tmp);

	/* Select Sample Time, bit number and ADC Reference */
	Stmpe811::writeRegister(STMPE811_ADC_CTRL1, 0x49);
	Stmpe811::delayMs(2);

	/* Select the ADC clock speed: 3.25 MHz */
	Stmpe811::writeRegister(STMPE811_ADC_CTRL2, 0x01);

	/* Select TSC pins in non default mode */
	Stmpe811::readRegisters(STMPE811_GPIO_AF, &tmp, 1);
	tmp |= 0x1E;
	Stmpe811::writeRegister(STMPE811_GPIO_AF, tmp);

	/* Select 2 nF filter capacitor */
	/* Configuration:
	- Touch average control    : 4 samples
	- Touch delay time         : 500 uS
	- Panel driver setting time: 500 uS
	*/
	Stmpe811::writeRegister(STMPE811_TSC_CFG, 0x9A);

	/* Configure the Touch FIFO threshold: single point reading */
	Stmpe811::writeRegister(STMPE811_FIFO_TH, 0x01);

	/* Clear the FIFO memory content. */
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x01);

	/* Put the FIFO back into operation mode  */
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x00);

	/* Set the range and accuracy pf the pressure measurement (Z) :
	- Fractional part :7
	- Whole part      :1
	*/
	Stmpe811::writeRegister(STMPE811_TSC_FRACTION_Z, 0x0F);

	/* Set the driving capability (limit) of the device for TSC pins: 50mA */
	Stmpe811::writeRegister(STMPE811_TSC_I_DRIVE, 0x01);

	/* Touch screen control configuration (enable TSC):
	- No window tracking index
	- XYZ acquisition mode
	*/
	Stmpe811::writeRegister(STMPE811_TSC_CTRL, 0x01);

	/* Clear all the status pending bits if any */
	Stmpe811::writeRegister(STMPE811_INT_STA, 0xFF);
	Stmpe811::delayMs(2);

	/* Check for driver connected */
	Stmpe811::readRegisters(STMPE811_CHIP_ID,Buffer,2);
	chipID = Buffer[0]<<8|Buffer[1];
	if (chipID != STMPE811_CHIP_ID_VALUE)
		return false;
	return true;
}

template<typename T>
void Stmpe811<T>::convertDriverDataToXYZ()
{
	x = (x * TouchPanel<T>::width) / STMPE911_MAX_RES;
	y = (y * TouchPanel<T>::height) / STMPE911_MAX_RES;
	//z = z;
}

template<typename T>
void Stmpe811<T>::resetFIFO()
{
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x01);
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x00);
}

template<typename T>
bool Stmpe811<T>::update()
{
	uint8_t val;

	/* Read */
	Stmpe811::readRegisters(STMPE811_TSC_CTRL, &val, 1);
	if ((val & 0x80) != 0)
	{
		/* Clear all the status pending bits if any */
		//Stmpe811::writeRegister(STMPE811_INT_STA, 0xFF);

		//Pressed
		uint8_t buffer[4];
		Stmpe811::readRegisters(STMPE811_TSC_DATA_XYZ, buffer, 4);

		//Reset Fifo
		Stmpe811::resetFIFO();

		//Check for valid data
		z = buffer[3];
		if(Stmpe811::isEnoughPressed())
		{
			y = (buffer[0] << 4) | (buffer[1] >> 4);
			x = ((buffer[1] & 0x0F) << 8) | buffer[2];
			Stmpe811::convertDriverDataToXYZ();
			TouchPanel<T>::pressState = TouchPanel<T>::calculateCoordinates( x, y, z );
		}
	}
	else
	{
		//Not pressed
		TouchPanel<T>::pressState = false;

		//Reset Fifo
		Stmpe811::resetFIFO();
	}

	return TouchPanel<T>::pressState;
}


#endif /* STMPE811_H_ */
