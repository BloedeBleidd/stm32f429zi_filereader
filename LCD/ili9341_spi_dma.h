/*
 * ili9341_spi_dma.h
 *
 *  Created on: Sep 8, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef Ili9341_SPI_DMA_H_
#define Ili9341_SPI_DMA_H_

#include "ili9341_spi.h"

template<typename Color, typename Pos>
class Ili9341_Spi_Dma :  public Ili9341_Spi<Color,Pos>
{
public:

protected:

public:


	void refresh();
};


#endif /* Ili9341_SPI_DMA_H_ */
