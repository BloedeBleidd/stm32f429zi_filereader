/*
 * ili9341_ltdc.cpp
 *
 *  Created on: Sep 8, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "ili9341_ltdc.h"
#include "stm32f4xx.h"

const Ili9341_LTDC::Ili9341_Spi::SpiInitStruct Ili9341_LTDC::spi{ SPI5, 5, GPIOC, 2, GPIOD, 13, GPIOF, 9, GPIOF, 7 };

const GPIO_TypeDef * Ili9341_LTDC::GPIOInitTable[23] =
{
	GPIOA, GPIOA, GPIOA, GPIOA, GPIOA,
	GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB,
	GPIOC, GPIOC, GPIOC,
	GPIOD, GPIOD,
	GPIOF,
	GPIOG, GPIOG, GPIOG, GPIOG, GPIOG,
	0
};

const uint8_t Ili9341_LTDC::PINInitTable[22] =
{
	3, 4, 6, 11, 12,
	0, 1, 8, 9, 10, 11,
	6, 7, 10,
	3, 6,
	10,
	6, 7, 10, 11, 12
};

const uint8_t Ili9341_LTDC::AFInitTable[22] =
{
	14, 14, 14, 14, 14,
	9, 9, 14, 14, 14, 14,
	14, 14, 14,
	14, 14,
	14,
	14, 14, 9, 14, 9
};

Ili9341_LTDC::Ili9341_LTDC( Orientation orientation, bool use2LayerBuffer, void *graphicBufferAddr, void *VRAM1Addr, void *VRAM2Addr )
: Ili9341_Spi<uint16_t,int32_t>()
{
	this->width = Ili9341::HEIGHT_MAX;
	this->height = Ili9341::WIDTH_MAX;
	this->orientation = Landscape_1;
	setOrientation( orientation );
	this->amountOfPixels = width*height;
	spiStruct = spi;

	this->createBuffer( graphicBufferAddr, this->graphicBuffer );
	createBuffer( VRAM1Addr, VRAM1 );

	if( use2LayerBuffer == true )
		createBuffer( VRAM2Addr, VRAM2 );

	Ili9341_LTDC::hardwareInitialize();

	// init spi
	RCC->APB2ENR |= RCC_APB2ENR_SPI5EN;

	// SCK, MOSI, DC, CS
	gpioPinConfiguration( spiStruct.gpioMosi, spiStruct.mosi, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED );
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioMosi, spiStruct.mosi, spiStruct.altFunction );
	gpioPinConfiguration( spiStruct.gpioClock, spiStruct.clock, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED );
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioClock, spiStruct.clock, spiStruct.altFunction );
	gpioPinConfiguration( spiStruct.gpioChipSelect, spiStruct.chipSelect, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED );
	gpioPinConfiguration( spiStruct.gpioDataCommand, spiStruct.dataCommand, GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED );

	// init spi
	spiStruct.spi->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	spiSlowMode();
	spiSetMode8Bit();

	Ili9341_LTDC::Ili9341Initialize();

}

Ili9341_LTDC::~Ili9341_LTDC()
{
	Graphic::deleteBuffer(VRAM1);
	Graphic::deleteBuffer(VRAM2);
}

void Ili9341_LTDC::hardwareInitialize()
{
	/* PLL  */
	RCC->PLLSAICFGR = (192 << RCC_PLLSAICFGR_PLLSAIN_Pos) | (7 << RCC_PLLSAICFGR_PLLSAIQ_Pos) | (4 << RCC_PLLSAICFGR_PLLSAIR_Pos);
	RCC->DCKCFGR &= ~RCC_DCKCFGR_PLLSAIDIVR;
	RCC->DCKCFGR |= RCC_DCKCFGR_PLLSAIDIVR_1;
	/* Enable SAI PLL */
	RCC->CR |= RCC_CR_PLLSAION;
	/* wait for SAI PLL ready */
	while((RCC->CR & RCC_CR_PLLSAIRDY) == 0){};

	/* enable clock for LTDC */
	RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;
	/* enable clock for DMA2D */
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2DEN;

	/* GPIO LTDC pin configuration */
	for( uint32_t i=0; GPIOInitTable[i] != 0; i++ )
	{
		gpioPinConfiguration( (GPIO_TypeDef * )GPIOInitTable[i], PINInitTable[i], GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED );
		gpioPinAlternateFunctionConfiguration( (GPIO_TypeDef * )GPIOInitTable[i], PINInitTable[i], (AlternateFunctionNumber)AFInitTable[i] );
	}

	/* disable LCD-TFT controller for re-initialisation */
	LTDC->GCR = 0;
	/* Synchronization Size Configuration */
	LTDC->SSCR = (ILI9341_HSYNC << LTDC_SSCR_HSW_Pos) | ILI9341_VSYNC;
	/* Back Porch Configuration */
	LTDC->BPCR = (ILI9341_HBP << LTDC_BPCR_AHBP_Pos) | ILI9341_VBP;
	/* Active Width Configuration */
	LTDC->AWCR = (ACTIVE_W << LTDC_AWCR_AAW_Pos) | (ACTIVE_H);
	/* Total Width Configuration */
	LTDC->TWCR = (TOTAL_WIDTH << LTDC_TWCR_TOTALW_Pos) | (TOTAL_HEIGHT);
	/* Background black */
	LTDC->BCCR = 0;

	/* Initialize layers */

	/* disable for reprogramming */
	LTDC_Layer1->CR = 0;
	/* Window Horizontal Position Configuration */
	LTDC_Layer1->WHPCR = (ILI9341_HBP + 1) | ((ILI9341_HBP + Ili9341::WIDTH_MAX) << LTDC_LxWHPCR_WHSPPOS_Pos);
	/* Window Vertical Position Configuration */
	LTDC_Layer1->WVPCR = (ILI9341_VBP + 1) | ((ILI9341_VBP + Ili9341::HEIGHT_MAX) << LTDC_LxWVPCR_WVSPPOS_Pos);
	/* Pixel Format Configuration */
	LTDC_Layer1->PFCR = 2; // rgb565
	/* Color Frame Buffer Address */
	LTDC_Layer1->CFBAR = (uint32_t)VRAM1;
	/* Color Frame Buffer Length */
	LTDC_Layer1->CFBLR = ((Ili9341::WIDTH_MAX * sizeof(uint16_t)) << LTDC_LxCFBLR_CFBP_Pos) | ((Ili9341::WIDTH_MAX * sizeof(uint16_t)) + 3);
	/* Color Frame Buffer Line */
	LTDC_Layer1->CFBLNR = Ili9341::HEIGHT_MAX;
	/* Black data is transparent, allowing background to be shown */
	LTDC_Layer1->CKCR = 0x000;

	if(VRAM2 != nullptr)
	{
		/* disable for reprogramming */
		LTDC_Layer2->CR = 0;
		/* Window Horizontal Position Configuration */
		LTDC_Layer2->WHPCR = (ILI9341_HBP + 1) | ((ILI9341_HBP + Ili9341::WIDTH_MAX) << LTDC_LxWHPCR_WHSPPOS_Pos);
		/* Window Vertical Position Configuration */
		LTDC_Layer2->WVPCR = (ILI9341_VBP + 1) | ((ILI9341_VBP + Ili9341::HEIGHT_MAX) << LTDC_LxWVPCR_WVSPPOS_Pos);
		/* Pixel Format Configuration */
		LTDC_Layer2->PFCR = 2; // rgb565
		/* Color Frame Buffer Address */
		LTDC_Layer2->CFBAR = (uint32_t)VRAM2;
		/* Color Frame Buffer Length */
		LTDC_Layer2->CFBLR = ((Ili9341::WIDTH_MAX * sizeof(uint16_t)) << LTDC_LxCFBLR_CFBP_Pos) | ((Ili9341::WIDTH_MAX * sizeof(uint16_t)) + 3);
		/* Color Frame Buffer Line */
		LTDC_Layer2->CFBLNR = Ili9341::HEIGHT_MAX;
		/* Black data is transparent, allowing background to be shown */
		LTDC_Layer2->CKCR = 0x000;
	}

	Ili9341_LTDC::reloadImmediate();

	/* Enable LTDC */
	LTDC->GCR = LTDC_GCR_LTDCEN;

	/* DMA M2M */
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
}

void Ili9341_LTDC::Ili9341Initialize()
{
	spiWriteCmd(0xCA);
	spiWriteData(0xC3);
	spiWriteData(0x08);
	spiWriteData(0x50);
	spiWriteCmd(POWERB);
	spiWriteData(0x00);
	spiWriteData(0xC1);
	spiWriteData(0x30);
	spiWriteCmd(POWER_SEQ);
	spiWriteData(0x64);
	spiWriteData(0x03);
	spiWriteData(0x12);
	spiWriteData(0x81);
	spiWriteCmd(DTCA);
	spiWriteData(0x85);
	spiWriteData(0x00);
	spiWriteData(0x78);
	spiWriteCmd(POWERA);
	spiWriteData(0x39);
	spiWriteData(0x2C);
	spiWriteData(0x00);
	spiWriteData(0x34);
	spiWriteData(0x02);
	spiWriteCmd(PRC);
	spiWriteData(0x20);
	spiWriteCmd(DTCB);
	spiWriteData(0x00);
	spiWriteData(0x00);
	spiWriteCmd(FRC);
	spiWriteData(0x00);
	spiWriteData(0x1B);
	spiWriteCmd(DFC);
	spiWriteData(0x0A);
	spiWriteData(0xA2);
	spiWriteCmd(POWER1);
	spiWriteData(0x10);
	spiWriteCmd(POWER2);
	spiWriteData(0x10);
	spiWriteCmd(VCOM1);
	spiWriteData(0x45);
	spiWriteData(0x15);
	spiWriteCmd(VCOM2);
	spiWriteData(0x90);
	spiWriteCmd(MAC);
	spiWriteData(0xC8);
	spiWriteCmd(GAMMA3_EN);
	spiWriteData(0x00);
	spiWriteCmd(RGB_INTERFACE);
	spiWriteData(0xC2);
	spiWriteCmd(DFC);
	spiWriteData(0x0A);
	spiWriteData(0xA7);
	spiWriteData(0x27);
	spiWriteData(0x04);

	spiWriteCmd(COLUMN_ADDR);
	spiWriteData(0x00);
	spiWriteData(0x00);
	spiWriteData(0x00);
	spiWriteData(0xEF);

	spiWriteCmd(PAGE_ADDR);
	spiWriteData(0x00);
	spiWriteData(0x00);
	spiWriteData(0x01);
	spiWriteData(0x3F);
	spiWriteCmd(INTERFACE);
	spiWriteData(0x01);
	spiWriteData(0x00);
	spiWriteData(0x06);

	spiWriteCmd(GRAM);
	delayMs( 100 );

	spiWriteCmd(GAMMA);
	spiWriteData(0x01);

	spiWriteCmd(PGAMMA);
	spiWriteData(0x0F);
	spiWriteData(0x29);
	spiWriteData(0x24);
	spiWriteData(0x0C);
	spiWriteData(0x0E);
	spiWriteData(0x09);
	spiWriteData(0x4E);
	spiWriteData(0x78);
	spiWriteData(0x3C);
	spiWriteData(0x09);
	spiWriteData(0x13);
	spiWriteData(0x05);
	spiWriteData(0x17);
	spiWriteData(0x11);
	spiWriteData(0x00);
	spiWriteCmd(NGAMMA);
	spiWriteData(0x00);
	spiWriteData(0x16);
	spiWriteData(0x1B);
	spiWriteData(0x04);
	spiWriteData(0x11);
	spiWriteData(0x07);
	spiWriteData(0x31);
	spiWriteData(0x33);
	spiWriteData(0x42);
	spiWriteData(0x05);
	spiWriteData(0x0C);
	spiWriteData(0x0A);
	spiWriteData(0x28);
	spiWriteData(0x2F);
	spiWriteData(0x0F);

	spiWriteCmd(SLEEP_OUT);
	delayMs( 100 );
	spiWriteCmd(DISPLAY_ON);

	spiWriteCmd(GRAM);
}

bool Ili9341_LTDC::layerSet(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			layerCurrent = l;
			return true;
		}
		return false;
	}
	layerCurrent = l;
	return true;
}

bool Ili9341_LTDC::layerChange()
{
	if(layerCurrent == BOTTOM)
	{
		if(isLayerTopUsed())
		{
			layerCurrent = TOP;
			return true;
		}
		return false;
	}
	layerCurrent = BOTTOM;
	return true;
}

bool Ili9341_LTDC::refreshLayer(Layer l)
{
	if(l == TOP)
		return Ili9341_LTDC::refreshTop();
	Ili9341_LTDC::refreshBottom();
	return true;
}

bool Ili9341_LTDC::refreshTop()
{
	if(!isLayerTopUsed())
		return false;
	Ili9341_LTDC::buffCopy32Bit(VRAM2, graphicBuffer, amountOfPixels );
	//memcpy(VRAM2,Graphic::bufferPointerGet(),pixelAmount*sizeof(Color));
	return true;
}

void Ili9341_LTDC::refreshBottom()
{
	Ili9341_LTDC::buffCopy32Bit(VRAM1, graphicBuffer, amountOfPixels );
	//memcpy(VRAM1,Graphic::bufferPointerGet(),pixelAmount*sizeof(Color));
}

void Ili9341_LTDC::refreshCurrentLayer()
{
	if(layerCurrent == TOP)
		Ili9341_LTDC::refreshTop();
	else
		Ili9341_LTDC::refreshBottom();
}

bool Ili9341_LTDC::refreshNotCurrentLayer()
{
	if(layerCurrent == BOTTOM)
		if(isLayerTopUsed())
			return Ili9341_LTDC::refreshTop();
	Ili9341_LTDC::refreshBottom();
	return true;
}

bool Ili9341_LTDC::layerOpacitySetTop(uint8_t o)
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CACR = o;
	Ili9341_LTDC::reloadImmediate();
	return true;
}

void Ili9341_LTDC::layerOpacitySetBottom(uint8_t o)
{
	LTDC_Layer1->CACR = o;
	Ili9341_LTDC::reloadImmediate();
}

void Ili9341_LTDC::layerOpacitySetCurrent(uint8_t o)
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOpacitySetTop(o);
	Ili9341_LTDC::layerOpacitySetBottom(o);

}

bool Ili9341_LTDC::layerOpacitySetNotCurrent(uint8_t o)
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOpacitySetTop(o);
	Ili9341_LTDC::layerOpacitySetBottom(o);
	return true;
}

bool Ili9341_LTDC::layerOpacityGetTop(uint8_t &o)
{
	if(!isLayerTopUsed())
		return false;
	o = LTDC_Layer2->CACR;
	return true;
}

void Ili9341_LTDC::layerOpacityGetBottom(uint8_t &o)
{
	o = LTDC_Layer1->CACR;
}

void Ili9341_LTDC::layerOpacityGetCurrent(uint8_t &o)
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOpacityGetTop(o);
	Ili9341_LTDC::layerOpacityGetBottom(o);

}

bool Ili9341_LTDC::layerOpacityGetNotCurrent(uint8_t &o)
{
	if(layerCurrent== TOP)
		return Ili9341_LTDC::layerOpacityGetTop(o);
	Ili9341_LTDC::layerOpacityGetBottom(o);
	return true;
}

bool Ili9341_LTDC::layerCopyFromTopToBottom()
{
	if(!isLayerTopUsed())
		return false;
	uint32_t len = width*height;
	for(uint32_t i=0; i<len; i++)
		VRAM1[i] = VRAM2[i];
	return true;
}

bool Ili9341_LTDC::layerCopyFromBottomToTop()
{
	if(!isLayerTopUsed())
		return false;
	uint32_t len = width*height;
	for(uint32_t i=0; i<len; i++)
		VRAM2[i] = VRAM1[i];
	return true;
}

bool Ili9341_LTDC::layerCopyFromCurrent()
{
	if(!isLayerTopUsed())
		return false;
	if(layerCurrent== TOP)
		Ili9341_LTDC::layerCopyFromTopToBottom();
	else
		Ili9341_LTDC::layerCopyFromBottomToTop();
	return true;
}

bool Ili9341_LTDC::layerCopyToCurrent()
{
	if(!isLayerTopUsed())
		return false;
	if(layerCurrent== TOP)
		Ili9341_LTDC::layerCopyFromBottomToTop();
	else
		Ili9341_LTDC::layerCopyFromTopToBottom();
	return true;
}

bool Ili9341_LTDC::layerOn(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			Ili9341_LTDC::layerOnTop();
			return true;
		}
		return false;
	}
	Ili9341_LTDC::layerOnBottom();
	return true;
}

bool Ili9341_LTDC::layerOnTop()
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CR |= LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
	return true;
}

void Ili9341_LTDC::layerOnBottom()
{
	LTDC_Layer1->CR |= LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
}

void Ili9341_LTDC::layerOnCurrent()
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOnBottom();
	else
		Ili9341_LTDC::layerOnTop();
}

bool Ili9341_LTDC::layerOnNotCurrent()
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOnTop();
	Ili9341_LTDC::layerOnBottom();
	return true;
}

bool Ili9341_LTDC::layerOff(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			Ili9341_LTDC::layerOffTop();
			return true;
		}
		return false;
	}
	Ili9341_LTDC::layerOffBottom();
	return true;
}

bool Ili9341_LTDC::layerOffTop()
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CR &= ~LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
	return true;
}

void Ili9341_LTDC::layerOffBottom()
{
	LTDC_Layer1->CR &= ~LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
}

void Ili9341_LTDC::layerOffCurrent()
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOffBottom();
	else
		Ili9341_LTDC::layerOffTop();
}

bool Ili9341_LTDC::layerOffNotCurrent()
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOffTop();
	Ili9341_LTDC::layerOffBottom();
	return true;
}

void Ili9341_LTDC::displayOn()
{
	/* Send command to display on */
	spiWriteCmd(DISPLAY_ON);
	/* Enable LTDC */
	LTDC->GCR |= LTDC_GCR_LTDCEN;
}

void Ili9341_LTDC::displayOff()
{
	/* Send command to display on */
	spiWriteCmd(DISPLAY_OFF);
	/* Enable LTDC */
	LTDC->GCR &= ~LTDC_GCR_LTDCEN;
}

void Ili9341_LTDC::buffCopy32Bit( void *dst, void const *src, uint32_t len )
{
	DMA2_Stream7->CR = 0;
	DMA2_Stream7->PAR = (uint32_t)src;
	DMA2_Stream7->M0AR = (uint32_t)dst;
	DMA2_Stream7->NDTR = len/2;
	DMA2_Stream7->FCR = DMA_SxFCR_DMDIS | DMA_SxFCR_FTH;
	DMA2_Stream7->CR = DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MBURST_0 | DMA_SxCR_PBURST_0  | DMA_SxCR_DIR_1 | ( 0 << DMA_SxCR_CHSEL_Pos );
	DMA2_Stream7->CR |= DMA_SxCR_EN;

	while ( ! (DMA2->HISR & DMA_HISR_TCIF7) )
		delayMs( 1 );

	DMA2->HIFCR |= DMA_HIFCR_CTCIF7;

//	memcpy( (uint32_t *)dst, (uint32_t const *)src, len );
}

/*
template< >
void Graphic<uint16_t,int32_t>::fill( uint16_t color )
{
	DMA2_Stream7->CR = 0;
	DMA2_Stream7->PAR = (uint32_t)src;
	DMA2_Stream7->M0AR = (uint32_t)dst;
	DMA2_Stream7->NDTR = len/2;
	DMA2_Stream7->FCR = DMA_SxFCR_DMDIS | DMA_SxFCR_FTH;
	DMA2_Stream7->CR = DMA_SxCR_MINC | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MBURST_0 | DMA_SxCR_PBURST_0  | DMA_SxCR_DIR_1 | ( 0 << DMA_SxCR_CHSEL_Pos );
	DMA2_Stream7->CR |= DMA_SxCR_EN;

	while ( ! (DMA2->HISR & DMA_HISR_TCIF7) )
		delayMs( 1 );

	DMA2->HIFCR |= DMA_HIFCR_CTCIF7;
}*/

template< >
void Graphic<uint16_t,int32_t>::drawPixel( int32_t x, int32_t y, uint16_t color )
{
	if( (x < 0) || (x >= width) || (y < 0) || (y >= height) )
		return;

	switch( orientation )
	{
	case Landscape_1:
		*(uint16_t *)(graphicBuffer + (x+1)*height-y-1 ) = color;
		break;

	case Landscape_2:
		*(uint16_t *)(graphicBuffer + ( (y-height)+(width-x)*height) ) = color;
		break;

	case Portrait_1:
		*(uint16_t *)(graphicBuffer + (x + width*y)) = color;
		break;

	case Portrait_2:
		*(uint16_t *)(graphicBuffer + (amountOfPixels - x - width*y) ) = color;
		break;
	}
}
