/*
 * touch_panel.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef TOUCH_PANEL_H_
#define TOUCH_PANEL_H_

#include <cstdint>
#include "orientation.h"

template<typename T>
class TouchPanel
{
public:
	struct Coordinates
	{
		T x,y,z;
	};

protected:
	T width, height, xMin, xMax, yMin, yMax, zMin;
	Coordinates coordinates;
	Orientation orientation;
	bool pressState;

	bool isEnoughPressed();
	bool calculateCoordinates( T x, T y, T z );

public:
	void calibrate( T x_min, T x_max, T y_min, T y_max );
	void setMinPressureForce( T force ) { zMin = force; }
	void resize( T width, T height, Orientation orientation );
	void resize( T width, T height );

	T getX() const;
	T getY() const;
	T getZ() const				{ return coordinates.z; }

	bool isPressed() const		{ return pressState; }
	bool isReleased() const		{ return !pressState; }
};

template< typename T>
void TouchPanel<T>::calibrate( T x_min, T x_max, T y_min, T y_max )
{
	xMin = x_min;
	xMax = x_max;
	yMin = y_min;
	yMax = y_max;
}

template< typename T>
T TouchPanel<T>::getX() const
{
	switch(orientation)
	{
	case Portrait_2:
		return height-coordinates.y;

	case Portrait_1:
		return coordinates.y;

	case Landscape_2:
		return width-coordinates.x;

	case Landscape_1:
		return coordinates.x;
	}
}

template< typename T>
T TouchPanel<T>::getY() const
{
	switch(orientation)
	{
	case Portrait_2:
		return coordinates.x;

	case Portrait_1:
		return width-coordinates.x;

	case Landscape_2:
		return height-coordinates.y;

	case Landscape_1:
		return coordinates.y;
	}
}

template< typename T>
void TouchPanel<T>::resize( T width, T height, Orientation orientation )
{
	this->width = width;
	this->height = height;
	this->orientation = orientation;
}

template< typename T>
void TouchPanel<T>::resize( T width, T height)
{
	this->width = width;
	this->height = height;
}

template< typename T>
bool TouchPanel<T>::isEnoughPressed()
{
	if( coordinates.z >= zMin )
		return true;
	return false;
}

template< typename T>
bool TouchPanel<T>::calculateCoordinates( T x, T y, T z )
{
	if( x > xMax || x < xMin || y > yMax || y < yMin )
		return false;

	coordinates.x = width * (x - xMin) / (xMax - xMin);
	coordinates.y = height * (y - yMin) / (yMax - yMin);
	coordinates.z = z;
	return true;
}

#endif /* TOUCH_PANEL_H_ */
