/*
 * bitmap.h
 *
 *  Created on: Sep 12, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef BITMAP_H_
#define BITMAP_H_

#include <stdint.h>

template<typename Color>
struct Bitmap
{
	uint16_t width;
	uint16_t height;
	const Color *data;
};


#endif /* BITMAP_H_ */
