/*
 * font.h
 *
 *  Created on: Sep 6, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef FONT_H_
#define FONT_H_

#include <stdint.h>

namespace Font
{
	struct Data
	{
		uint8_t width;
		uint8_t height;
		uint8_t first;
		uint8_t last;
		const uint8_t *data;
	};

	extern const Data Font7x10;
	extern const Data Font11x18;
	extern const Data Font16x26;
	extern const Data Font_Comic_Sans_MS20x28;
	extern const Data Font_Bahnschrift_Condensed7x12;
};


#endif /* FONT_H_ */
