/*
 * graphic.h
 *
 *  Created on: Sep 6, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef GRAPHIC_H_
#define GRAPHIC_H_

#include <string>
#include <cstring>
#include <algorithm>
#include <cmath>
#include "font.h"
#include "bitmap.h"
#include "orientation.h"

template<typename Color, typename Pos>
class Graphic
{
public:
	enum Str2IntBase
	{
		DECIMAL = 10,
		HEXADECIMAL = 16,
		BINARY = 2
	};

	enum Corner
	{
		FIRST,
		SECOND,
		THIRD,
		FOURTH
	};

private:
	Pos xCursor = 0, yCursor = 0;
	Str2IntBase intBase = DECIMAL;

protected:
	Color *graphicBuffer;
	Orientation orientation;
	Pos width, height;
	Pos amountOfPixels;
	Font::Data font;
	uint8_t floatPrecision;
	Str2IntBase int2StrBase = DECIMAL;

	Color * createBuffer						( void *address, Color *&buffer );
	void deleteBuffer							( Color *&buffer );
public:
	void drawPixel								( Pos x, Pos y, Color color );

	void drawLine								( Pos xBeg, Pos yBeg, Pos xEnd, Pos yEnd, Color color );
	void drawLineVertical						( Pos x, Pos yBeg, Pos yEnd, Color color );
	void drawLineHorizontal						( Pos xBeg, Pos xEnd, Pos y, Color color );

	void drawRectangle							( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, Color color );
	void drawRectangleAndFill					( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, Color color );
	void drawRectangleWithRoundedEdges			( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, uint16_t r, Color color );
	void drawRectangleWithRoundedEdgesAndFill	( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, uint16_t r, Color color );

	void drawCircle								( Pos x, Pos y, uint16_t r, Color color );
	void drawCircleAndFill						( Pos x, Pos y, uint16_t r, Color color );
	void drawCircleCorner						( Pos x, Pos y, uint16_t r, Corner corner, Color color );
	void drawCircleCornerAndFill				( Pos x, Pos y, uint16_t r, Corner corner, Color color );

	void drawTriangle							( Pos xA, Pos yA, Pos xB, Pos yB, Pos xC, Pos yC, Color color );
	void drawTriangleAndFill					( Pos xA, Pos yA, Pos xB, Pos yB, Pos xC, Pos yC, Color color );

	void drawChar								( Pos x, Pos y, char ch, Color color );
	void drawChar								( char ch, Color color );
	void drawString								( Pos x, Pos y, const char *str, Color color );
	void drawString								( const char *str, Color color );
	void drawString								( Pos x, Pos y, const std::string &str, Color color );
	void drawString								( const std::string &str, Color color );
	void drawInteger							( Pos x, Pos y, int32_t data, Color color );
	void drawInteger							( int32_t data, Color color );
	void drawFloat								( Pos x, Pos y, float data, Color color );
	void drawFloat								( float data, Color color );
	void drawFloat								( Pos x, Pos y, float data, uint8_t precision, Color color );
	void drawFloat								( float data, uint8_t precision, Color color );

	void drawData								( Pos x, Pos y, const char ch, Color color );
	void drawData								( const char ch, Color color );
	void drawData								( Pos x, Pos y, const char *str, Color color );
	void drawData								( const char *str, Color color );
	void drawData								( Pos x, Pos y, const std::string &str, Color color );
	void drawData								( const std::string &str, Color color );
	void drawData								( Pos x, Pos y, int32_t data, Color color );
	void drawData								( int32_t data, Color color );
	void drawData								( Pos x, Pos y, float data, Color color );
	void drawData								( float data, Color color );
	void drawData								( Pos x, Pos y, float data, uint8_t precision, Color color );
	void drawData								( float data, uint8_t precision, Color color );

	void drawBitmap								( Pos x, Pos y, const Color *, Pos width, Pos height );
	void drawBitmap								( Pos x, Pos y, const Bitmap<Color> &bitmap );

	void fill									( Color color );

	void setFont( Font::Data font )						{ this->font = font; }
	Font::Data getFont()								{ return font; }
	uint8_t getFontWidth()								{ return font.width; }
	uint8_t getFontWidth( Font::Data font )				{ return font.width; }
	uint8_t getFontHeight()								{ return font.height; }
	uint8_t getFontHeight( Font::Data font )			{ return font.height; }

	uint16_t getBitmapWidth( Bitmap<Color> &bitmap )	{ return bitmap.width; }
	uint16_t getBitmapHeight( Bitmap<Color> &bitmap )	{ return bitmap.height; }

	Pos getX()											{ return xCursor; }
	Pos getY()											{ return yCursor; }
	void setX( Pos x )									{ this->xCursor = x; }
	void setY( Pos y )									{ this->yCursor = y; }
	void setXY( Pos x, Pos y )							{ this->xCursor = x; this->yCursor = y; }

	void setOrientation( Orientation o );
	Orientation getOrientation()						{ return orientation; }

	void setFloatPrecision( uint8_t p )					{ floatPrecision = p; }
	uint8_t getFloatPrecision()							{ return floatPrecision; }

	void setInt2StringBase( Str2IntBase b )				{ int2StrBase = b; }
	Str2IntBase baseStringGet()							{ return int2StrBase; }

	Pos getWidth()										{ return width; }
	Pos getHeight()										{ return height; }

	Color * getBufferPointer()							{ return graphicBuffer; }
};

template<typename Color, typename Pos>
Color * Graphic<Color,Pos>::createBuffer( void *address, Color *&buffer )
{
	if( address != nullptr )
		buffer = new (address) Color [amountOfPixels];
	else
		buffer = new Color [amountOfPixels];

	//fill( 0 );

	return buffer;
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::deleteBuffer( Color *&buffer )
{
	delete[] buffer;
	buffer = nullptr;
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawPixel( Pos x, Pos y, Color color )
{
	if( (x < 0) || (x >= width) || (y < 0) || (y >= height) )
		return;

	switch( orientation )
	{
	case Portrait_1:
		*(Color *)(graphicBuffer + y + height*( width-x ) ) = color;
		break;

	case Portrait_2:
		*(Color *)(graphicBuffer - y + height*( x + 1 ) ) = color;
		break;

	case Landscape_1:
		*(Color *)(graphicBuffer + x + y*width ) = color;
		break;

	case Landscape_2:
		*(Color *)(graphicBuffer + amountOfPixels - x - y*width ) = color;
		break;
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawLine( Pos xBeg, Pos yBeg, Pos xEnd, Pos yEnd, Color color )
{
	using std::swap;

	Pos dx, dy;
	Pos ystep;
	Pos err;
	bool steep = false;

	if( ( abs( yEnd - yBeg ) ) > ( abs( xEnd - xBeg ) ) )
		steep = true;

	if( true == steep )
	{
		swap( xBeg, yBeg );
		swap( xEnd, yEnd );
	}

	if( xBeg > xEnd )
	{
		swap( xBeg, xEnd );
		swap( yBeg, yEnd );
	}

	dx = xEnd - xBeg;
	dy = abs( yEnd - yBeg );
	err = dx / 2;

	if( yBeg < yEnd )
		ystep = 1;
	else
		ystep = -1;

	for( ; xBeg <= xEnd; xBeg++ )
	{
		if( true == steep )
			drawPixel( yBeg, xBeg, color );
		else
			drawPixel( xBeg, yBeg, color );

		err -= dy;

		if( err < 0 )
		{
			yBeg += ystep;
			err += dx;
		}
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawLineVertical( Pos x, Pos yBeg, Pos yEnd, Color color )
{
	if( yBeg < yEnd )
		for( Pos i=yBeg; i<yEnd; i++ )
			drawPixel( x, i, color );
	else
		for( Pos i=yBeg; i>yEnd; i-- )
			drawPixel( x, i, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawLineHorizontal( Pos xBeg, Pos xEnd, Pos y, Color color )
{
	if( xBeg < xEnd )
		for( Pos i=xBeg; i<xEnd; i++ )
			drawPixel( i, y, color );
	else
		for( Pos i=xBeg; i>xEnd; i-- )
			drawPixel( i, y, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawRectangle( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, Color color )
{
	drawLineVertical( xStart, yStart, yEnd, color );
	drawLineVertical( xEnd, yStart, yEnd, color );
	drawLineHorizontal( xStart, xEnd, yStart, color );
	drawLineHorizontal( xStart, xEnd, yEnd, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawRectangleAndFill( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, Color color )
{
	for( Pos i=xStart; i<xEnd; i++ )
		drawLineVertical( i, yStart, yEnd, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawRectangleWithRoundedEdges( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, uint16_t r, Color color )
{
	using std::swap;

	if( xEnd == xStart || yEnd == yStart )
		return;

	if( r == 0 )
	{
		drawRectangle( xStart, yStart, xEnd, yEnd, color );
		return;
	}

	if( xStart > xEnd )
		swap( xStart, xEnd );

	if( yStart > yEnd )
		swap( yStart, yEnd );

	if( r > ((xEnd - xStart) / 2) )
		r = (xEnd - xStart) / 2;
	if( r > ((yEnd - yStart) / 2) )
		r = (yEnd - yStart) / 2;

	/* Draw lines */
	drawLine( xStart + r, yStart, xEnd - r, yStart, color ); /* Top */
	drawLine( xStart, yStart + r, xStart, yEnd - r, color );	/* Left */
	drawLine( xEnd, yStart + r, xEnd, yEnd - r, color );	/* Right */
	drawLine( xStart + r, yEnd, xEnd - r, yEnd, color );	/* Bottom */

	/* Draw corners */
	drawCircleCorner( xStart + r, yStart + r, r, FIRST, color ); /* Top left */
	drawCircleCorner( xEnd - r, yStart + r, r, SECOND, color ); /* Top right */
	drawCircleCorner( xEnd - r, yEnd - r, r, THIRD, color ); /* Bottom right */
	drawCircleCorner( xStart + r, yEnd - r, r, FOURTH, color ); /* Bottom left */
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawRectangleWithRoundedEdgesAndFill( Pos xStart, Pos yStart, Pos xEnd, Pos yEnd, uint16_t r, Color color )
{
	using std::swap;

	if( xEnd == xStart || yEnd == yStart )
		return;

	if( r == 0 )
	{
		drawRectangleAndFill( xStart, yStart, xEnd, yEnd, color );
		return;
	}

	if( xStart > xEnd )
		swap( xStart, xEnd );

	if( yStart > yEnd )
		swap( yStart, yEnd );

	if( r > ((xEnd - xStart) / 2) )
		r = (xEnd - xStart) / 2;
	if( r > ((yEnd - yStart) / 2) )
		r = (yEnd - yStart) / 2;

	/* Draw rectangles */
	drawRectangleAndFill(xStart + r, yStart, xEnd - r, yEnd, color);
	Graphic::drawRectangleAndFill(xStart, yStart + r, xStart + r, yEnd - r, color);
	Graphic::drawRectangleAndFill(xEnd - r, yStart + r, xEnd, yEnd - r, color);

	/* Draw corners */
	Graphic::drawCircleCornerAndFill(xStart + r, yStart + r, r, FIRST, color);
	Graphic::drawCircleCornerAndFill(xEnd - r, yStart + r, r, SECOND, color);
	Graphic::drawCircleCornerAndFill(xEnd - r, yEnd - r - 1, r, THIRD, color);
	Graphic::drawCircleCornerAndFill(xStart + r, yEnd - r - 1, r, FOURTH, color);
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawCircle( Pos x, Pos y, uint16_t r, Color color )
{
	Pos f = 1 - r;
	Pos ddF_x = 1;
	Pos ddF_y = -2 * r;
	Pos xTmp = 0;
	Pos yTmp = r;

	Graphic::drawPixel( x, y + r, color );
    Graphic::drawPixel( x, y - r, color );
    Graphic::drawPixel( x + r, y, color );
    Graphic::drawPixel( x - r, y, color );

    while( xTmp < yTmp )
    {
        if( f >= 0 )
        {
        	yTmp--;
            ddF_y += 2;
            f += ddF_y;
        }

        xTmp++;
        ddF_x += 2;
        f += ddF_x;

        Graphic::drawPixel( x + xTmp, y + yTmp, color );
        Graphic::drawPixel( x - xTmp, y + yTmp, color );
        Graphic::drawPixel( x + xTmp, y - yTmp, color );
        Graphic::drawPixel( x - xTmp, y - yTmp, color );

        Graphic::drawPixel( x + yTmp, y + xTmp, color );
        Graphic::drawPixel( x - yTmp, y + xTmp, color );
        Graphic::drawPixel( x + yTmp, y - xTmp, color );
        Graphic::drawPixel( x - yTmp, y - xTmp, color );
    }
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawCircleAndFill( Pos x, Pos y, uint16_t r, Color color )
{
	Pos f = 1 - r;
	Pos ddF_x = 1;
	Pos ddF_y = -2 * r;
	Pos xTmp = 0;
	Pos yTmp = r;

	Graphic::drawPixel( x, y + r, color );
	Graphic::drawPixel( x, y - r, color );
    Graphic::drawPixel( x + r, y, color );
    Graphic::drawPixel( x - r, y, color );
    Graphic::drawLine( x - r, y, x + r, y, color );

	while( xTmp < yTmp )
	{
		if( f >= 0 )
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		Graphic::drawLine( x - xTmp, y + yTmp, x + xTmp, y + yTmp, color );
		Graphic::drawLine( x + xTmp, y - yTmp, x - xTmp, y - yTmp, color );
		Graphic::drawLine( x + yTmp, y + xTmp, x - yTmp, y + xTmp, color );
		Graphic::drawLine( x + yTmp, y - xTmp, x - yTmp, y - xTmp, color );
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawCircleCorner( Pos x, Pos y, uint16_t r, Corner corner, Color color )
{
	Pos f = 1 - r;
	Pos ddF_x = 1;
	Pos ddF_y = -2 * r;
	Pos xTmp = 0;
	Pos yTmp = r;

	while( xTmp < yTmp )
	{
		if( f >= 0 )
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		switch( corner )
		{
		case FIRST:
			Graphic::drawPixel( x - yTmp, y - xTmp, color );
			Graphic::drawPixel( x - xTmp, y - yTmp, color );
			break;
		case SECOND:
			Graphic::drawPixel( x + xTmp, y - yTmp, color );
			Graphic::drawPixel( x + yTmp, y - xTmp, color );
			break;
		case THIRD:
			Graphic::drawPixel( x + xTmp, y + yTmp, color );
			Graphic::drawPixel( x + yTmp, y + xTmp, color );
			break;
		case FOURTH:
			Graphic::drawPixel( x - xTmp, y + yTmp, color );
			Graphic::drawPixel( x - yTmp, y + xTmp, color );
			break;
		}
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawCircleCornerAndFill( Pos x, Pos y, uint16_t r, Corner corner, Color color )
{
	Pos f = 1 - r;
	Pos ddF_x = 1;
	Pos ddF_y = -2 * r;
	Pos xTmp = 0;
	Pos yTmp = r;

	while( xTmp < yTmp )
	{
		if( f >= 0 )
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		switch( corner )
		{
		case FIRST:
			Graphic::drawLine( x, y - yTmp, x - xTmp, y - yTmp, color );
			Graphic::drawLine( x, y - xTmp, x - yTmp, y - xTmp, color );
			break;
		case SECOND:
			Graphic::drawLine( x + xTmp, y - yTmp, x, y - yTmp, color );
			Graphic::drawLine( x + yTmp, y - xTmp, x, y - xTmp, color );
			break;
		case THIRD:
			Graphic::drawLine( x, y + yTmp, x + xTmp, y + yTmp, color );
			Graphic::drawLine( x + yTmp, y + xTmp, x, y + xTmp, color );
			break;
		case FOURTH:
			Graphic::drawLine( x - xTmp, y + yTmp, x, y + yTmp, color );
			Graphic::drawLine( x, y + xTmp, x - yTmp, y + xTmp, color );
			break;
		}
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawTriangle( Pos xA, Pos yA, Pos xB, Pos yB, Pos xC, Pos yC, Color color )
{
	Graphic::drawLine( xA, yA, xB, yB, color );
	Graphic::drawLine( xB, yB, xC, yC, color );
	Graphic::drawLine( xA, yA, xC, yC, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawTriangleAndFill( Pos xA, Pos yA, Pos xB, Pos yB, Pos xC, Pos yC, Color color)
{
	using std::swap;
    Pos a, b, y, last;

    // Sort coordinates by Y order (yC >= yB >= yA)
    if( yA > yB )
    {
        swap( yA, yB );
        swap( xA, xB );
    }

    if( yB > yC )
    {
        swap( yC, yB );
        swap( xC, xB );
    }

    if( yA > yB )
    {
        swap( yA, yB );
        swap( xA, xB );
    }

    if( yA == yC )
    { // Handle awkward all-on-same-line case as its own thing
        a = b = xA;

        if( xB < a )
        	a = xB;
        else if( xB > b )
        	b = xB;

        if( xC < a )
        	a = xC;
        else if( xC > b )
        	b = xC;

        Graphic::drawLineHorizontal( a, b, yA, color );
        return;
    }

    Pos
    dxA1 = xB - xA,
    dyA1 = yB - yA,
    dxA2 = xC - xA,
    dyA2 = yC - yA,
    dxB2 = xC - xB,
    dyB2 = yC - yB;

    Pos
    sa = 0,
    sb = 0;

    // For upper part of triangle, find scanline crossings for segments
    // 0-1 and 0-2.  If yB=yC (flat-bottomed triangle), the scanline yB
    // is included here (and second loop will be skipped, avoiding a /0
    // error there), otherwise scanline yB is skipped here and handled
    // in the second loop...which also avoids a /0 error here if yA=yB
    // (flat-topped triangle).
    if( yB == yC )
    	last = yB;   // Include yB scanline
    else
    	last = yB-1; // Skip it

    for( y=yA; y<=last; y++ )
    {
        a   = xA + sa / dyA1;
        b   = xA + sb / dyA2;
        sa += dxA1;
        sb += dxA2;
        /* longhand:
        a = xA + (xB - xA) * (y - yA) / (yB - yA);
        b = xA + (xC - xA) * (y - yA) / (yC - yA);
        */
        if( a > b )
        	swap( a, b );

        Graphic::drawLineHorizontal( a, b, y, color );
    }

    // For lower part of triangle, find scanline crossings for segments
    // 0-2 and 1-2.  This loop is skipped if yB=yC.
    sa = dxB2 * (y - yB);
    sb = dxA2 * (y - yA);

    for( ; y<=yC; y++ )
    {
        a   = xB + sa / dyB2;
        b   = xA + sb / dyA2;
        sa += dxB2;
        sb += dxA2;
        /* longhand:
        a = xB + (xC - xB) * (y - yB) / (yC - yB);
        b = xA + (xC - xA) * (y - yA) / (yC - yA);
        */
        if( a > b )
        	swap( a, b );

        Graphic::drawLineHorizontal(a, b, y, color);
    }
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawChar( Pos x, Pos y, char ch, Color color )
{
	xCursor = x;
	yCursor = y;
	drawChar( ch, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawChar( char ch, Color color )
{
	if( xCursor >= width || yCursor >= height || xCursor+font.width < 0 || yCursor+font.height < 0 )
	{
		xCursor += font.width;
		return;
	}

	if( (uint8_t)(ch) > font.last || (uint8_t)(ch) < font.first )
		ch = (char)font.first;

	uint32_t letterBitStartPosition = ( (uint8_t)(ch) - font.first ) * font.width * font.height;

	for( Pos y = 0; y < font.height; y++ )
		for( Pos x = 0; x < font.width; x++ )
		{
			uint32_t currentBit = (letterBitStartPosition+x+y*font.width);
			uint32_t currentByte = currentBit/8;
			uint32_t bitInCurrentByte = currentBit%8;

			if( *(font.data+currentByte) & ( 1 << ( 7-bitInCurrentByte ) ) )
				drawPixel( x + xCursor, y + yCursor, color );
		}

	xCursor += font.width;
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawString( Pos x, Pos y, const char *str, Color color )
{
	xCursor = x;
	yCursor = y;
	drawString( str, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawString( const char *str, Color color )
{
	Pos initX = xCursor;

	while( *str )
	{
		// New line
		if( *str == '\n' )
		{
			yCursor += font.height;
			// if after \n is also \r, than go to the left of the screen
			if( *(str + 1) == '\r' )
			{
				xCursor = 0;
				str++;
			}
			// else xCursor equals initial x value
			else
				xCursor = initX;

			str++;
			continue;
		}
		else if( *str == '\r' )
		{
			str++;
			continue;
		}

		Graphic::drawChar( xCursor, yCursor, *str++, color );
	}
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawString( Pos x, Pos y, const std::string &str, Color color )
{
	Graphic::drawString( x, y, str.c_str(), color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawString( const std::string &str, Color color )
{
	Graphic::drawString( str.c_str(), color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawInteger( Pos x, Pos y, int32_t data, Color color )
{
	xCursor = x;
	yCursor = y;
	drawInteger( data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawInteger( int32_t data, Color color )
{
	char buf[32];

	itoa( data, buf, int2StrBase );
	Graphic::drawString( buf, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawFloat( Pos x, Pos y, float data, Color color )
{
	xCursor = x;
	yCursor = y;
	drawFloat( data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawFloat( float data, Color color )
{
	drawFloat( data, floatPrecision, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawFloat( Pos x, Pos y, float data, uint8_t precision, Color color )
{
	xCursor = x;
	yCursor = y;
	drawFloat( data, precision, color );
}

template<typename Color, typename Pos>
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}

template<typename Color, typename Pos>
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse<Color,Pos>(str, i);
    str[i] = '\0';
    return i;
}

template<typename Color, typename Pos>
void ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr<Color,Pos>(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr<Color,Pos>((int)fpart, res + i + 1, afterpoint);
    }
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawFloat( float data, uint8_t precision,Color color )
{
	char buf[32];

	ftoa<Color,Pos>( data, buf, precision );
	Graphic::drawString( buf, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, const char ch, Color color )
{
	Graphic::drawChar( x, y, ch, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( const char ch, Color color )
{
	Graphic::drawChar( ch, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, const char *str, Color color )
{
	Graphic::drawString( x, y, str, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( const char *str, Color color )
{
	Graphic::drawString( str, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, const std::string &str, Color color )
{
	Graphic::drawString( x, y, str, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( const std::string &str, Color color )
{
	Graphic::drawString( str, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, int32_t data, Color color )
{
	Graphic::drawInteger( x, y, data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( int32_t data, Color color )
{
	Graphic::drawInteger( data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, float data, Color color )
{
	Graphic::drawFloat( x, y, data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData(  float data, Color color )
{
	Graphic::drawFloat(  data, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( Pos x, Pos y, float data, uint8_t precision, Color color )
{
	Graphic::drawFloat( x, y, data, precision, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawData( float data, uint8_t precision, Color color )
{
	Graphic::drawFloat(  data, precision, color );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawBitmap( Pos x, Pos y, const Color *pixel, Pos width, Pos height )
{
	for( Pos i=y; i<height+y; i++ )
		for( Pos j=x; j<width+x; j++ )
			Graphic::drawPixel( j, i, *pixel++ );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::drawBitmap( Pos x, Pos y, const Bitmap<Color> &bitmap )
{
	const Color *data = bitmap.data;

	for( Pos i=y; i<bitmap.height+y; i++ )
		for( Pos j=x; j<bitmap.width+x; j++ )
			Graphic::drawPixel( j, i, *data++ );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::fill( Color color )
{
	memset( (Color *)graphicBuffer, color, amountOfPixels*sizeof( Color ) );
}

template<typename Color, typename Pos>
void Graphic<Color,Pos>::setOrientation( Orientation o )
{
	if( ( ( o == Landscape_1 || o == Landscape_2 ) && ( orientation == Portrait_1  || orientation == Portrait_2  ) ) \
	 || ( ( o == Portrait_1  || o == Portrait_2  ) && ( orientation == Landscape_1 || orientation == Landscape_2 ) ) )
		std::swap( this->width, this->height );

	orientation = o;
}

#endif /* GRAPHIC_H_ */
