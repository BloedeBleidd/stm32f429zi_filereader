/*
 * orientation.h
 *
 *  Created on: Sep 6, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef ORIENTATION_H_
#define ORIENTATION_H_


enum Orientation
{
	Portrait_1,
	Portrait_2,
	Landscape_1,
	Landscape_2
};


#endif /* ORIENTATION_H_ */
