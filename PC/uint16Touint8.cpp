
#include <string>
#include <fstream>
#include <streambuf>
#include <iterator>
#include <sstream>
#include <vector>
#include <iostream>
#include <stdlib.h>


void strSeparateComma( const std::string &str, std::vector<std::string> &vect )
{
	std::stringstream ss( str );

	while( ss.good() )
	{
	    std::string substr;
	    getline( ss, substr, ',' );
	    vect.push_back( substr );
	}
}

void strHexToUint16( const std::vector<std::string> &vectStr, std::vector<uint16_t> &vect16 )
{
	for( auto i = vectStr.begin(); i != vectStr.end(); i++ )
		vect16.push_back( strtoul( (*i).c_str(), NULL, 16 ) );
}

void splitUint16ToBits( const std::vector<uint16_t> &vect16, std::vector<uint8_t> &vectBit )
{
	for( auto it = vect16.begin(); it != vect16.end(); it++ )
	{
		for( int i=0; i<16; i++ )
		{
			if( *it & ( 1 << ( 15-i ) ) )
				vectBit.push_back( 1 );
			else
				vectBit.push_back( 0 );
		}
	}
}

void uint16ToResultUint8( const std::vector<uint16_t> &vect16, std::vector<uint8_t> &vectResult, uint16_t w, uint16_t h )
{
	uint16_t result = 0;
	int resultBit = 7;

	for( unsigned int num=0; num<vect16.size(); num++ )
	{
		for( int bit=15; bit>15-w; bit--, resultBit-- )
		{
			if( resultBit < 0 )
			{
				resultBit = 7;
				vectResult.push_back( result );
				result = 0;
			}

			if( vect16[num] & ( 1 << bit ) )
				result += 1 << resultBit;
		}
	}

	vectResult.push_back( result );
}

void resultUint8ToString( const std::vector<uint8_t> &vectResult, std::string &str )
{
	int cnt = 1;
	str.clear();

	for( auto i = vectResult.begin(); i != vectResult.end(); i++, cnt++ )
	{
		std::stringstream stream;
		stream << std::hex << int(*i);

		str.append( "0x" );
		str.append( stream.str() );
		str.append( ", " );

		if( ( cnt % 20 ) == 0 )
			str += "\n";
	}
}

template<typename T>
void printVect( const std::vector<T> &vect )
{
	for( auto i = vect.begin(); i != vect.end(); i++ )
		std::cout << "0x" << std::hex << (int)(*i) << ",";
	std::cout << std::endl;
}

template<>
void printVect( const std::vector<std::string> &vect )
{
	for( auto i = vect.begin(); i != vect.end(); i++ )
		std::cout << (*i) << ",";
	std::cout << std::endl;
}

int main( int argc,  char** argv )
{
	if( argc != 2+1 )
	{
		std::cout << "Bad arguments!!!";
		return 0;
	}

	int w = std::stoi(argv[1]), h = std::stoi(argv[2]);

	std::ifstream t( "input.txt" );
	std::string strInput( ( std::istreambuf_iterator<char>(t) ), std::istreambuf_iterator<char>() );
	std::vector<std::string> vectInputStr;
	std::vector<uint16_t> vectInput16;
	std::vector<uint8_t> vectInputBits;
	std::vector<uint8_t> vectResult8;
	std::string strResult;

	strSeparateComma( strInput, vectInputStr );
	printVect( vectInputStr );

	strHexToUint16( vectInputStr, vectInput16 );
	printVect( vectInput16 );
	std::cout << "dlugosc=" << std::dec << vectInput16.size() << std::endl;

	uint16ToResultUint8( vectInput16, vectResult8, w, h );
	printVect( vectResult8 );

	resultUint8ToString( vectResult8, strResult );
	std::cout << strResult << std::endl;

	std::cout << "dlugosc=" << std::dec << vectResult8.size() << std::endl;

	std::ofstream out( "output.txt" );
	out << strResult;
	out.close();

	return 0;
}
