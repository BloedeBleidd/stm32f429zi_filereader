/*
 * audio.h
 *
 *  Created on: Sep 19, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef AUDIO_H_
#define AUDIO_H_

#include <stdint.h>

enum AudioChannels { MONO, STEREO };
typedef uint16_t AudioData;

const uint8_t AUDIO_BITS = 12;
const uint16_t AUDIO_BUFFER_SIZE = 2048;

void audioInit( uint32_t sampling, AudioChannels ch );
void audioReset( void );
void audioFillBuffer( AudioData *data );
bool audioIsReady( void );
void audioStop( void );
void audioStart( void );

#endif /* AUDIO_H_ */
