/*
 * audio.cpp
 *
 *  Created on: Sep 19, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "audio.h"
#include <stddef.h>
#include "stm32f4xx.h"
#include "gpio.h"
#include <cstring>


#define AUDIO_SAMPLING_TIMER	TIM7
#define AUDIO_DAC_TIMER			TIM3
#define AUDIO_DAC_CH_LEFT		CCR1
#define AUDIO_DAC_CH_RIGHT		CCR2
#define AUDIO_GPIO_LEFT			GPIOB
#define AUDIO_GPIO_RIGHT		GPIOA
#define AUDIO_PIN_LEFT			4
#define AUDIO_PIN_RIGHT			7

enum AudioPart { LOW, HIGH };

static AudioData bufferLeft[AUDIO_BUFFER_SIZE*2], bufferRight[AUDIO_BUFFER_SIZE*2];
static volatile uint32_t bufferIndex = 0;
static AudioChannels channels;
static AudioPart part;

extern "C"
{
	void TIM7_IRQHandler( void )
	{
		AUDIO_SAMPLING_TIMER->SR = 0;

		AUDIO_DAC_TIMER->AUDIO_DAC_CH_LEFT = *(bufferLeft+bufferIndex);
		AUDIO_DAC_TIMER->AUDIO_DAC_CH_RIGHT = *(bufferRight+bufferIndex);

		bufferIndex++;
		if( bufferIndex >= AUDIO_BUFFER_SIZE*2 )
			bufferIndex = 0;
	}
}

void audioInit( uint32_t sampling, AudioChannels ch )
{
	audioReset();

	channels = ch;
	part = AudioPart::LOW;

	RCC->APB1ENR |= RCC_APB1ENR_TIM7EN | RCC_APB1ENR_TIM3EN;

	gpioPinConfiguration( AUDIO_GPIO_LEFT, AUDIO_PIN_LEFT, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED );
	gpioPinConfiguration( AUDIO_GPIO_RIGHT, AUDIO_PIN_RIGHT, GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED );

	AUDIO_DAC_TIMER->CR1 = TIM_CR1_ARPE;
	AUDIO_DAC_TIMER->CCMR1 = TIM_CCMR1_OC1PE | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC2PE | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1; //PWM Mode 1 & buffering ccr for ch1 & ch2
	AUDIO_DAC_TIMER->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E; //Enable output
	AUDIO_DAC_TIMER->PSC = 0;
	AUDIO_DAC_TIMER->ARR = (1<<AUDIO_BITS)-1;
	AUDIO_DAC_TIMER->EGR = TIM_EGR_UG;

	AUDIO_SAMPLING_TIMER->CR1 = TIM_CR1_ARPE | TIM_CR1_URS;
	AUDIO_SAMPLING_TIMER->DIER = TIM_DIER_UIE;
	AUDIO_SAMPLING_TIMER->PSC = 0;
	AUDIO_SAMPLING_TIMER->ARR = SystemCoreClock/sampling-1;
	AUDIO_SAMPLING_TIMER->EGR = TIM_EGR_UG;

	NVIC_EnableIRQ( TIM7_IRQn );
	NVIC_SetPriority( TIM7_IRQn, 7 );
}

void audioReset( void )
{
	audioStop();

	NVIC_ClearPendingIRQ( TIM7_IRQn );
	NVIC_DisableIRQ( TIM7_IRQn );

	part = AudioPart::LOW;
	bufferIndex = 0;
}

void audioFillBuffer( AudioData *data )
{
	AudioData *posL, *posR;

	if( part == AudioPart::HIGH )
	{
		posL = bufferLeft+AUDIO_BUFFER_SIZE;
		posR = bufferRight+AUDIO_BUFFER_SIZE;
		part = AudioPart::LOW;
	}
	else
	{
		posL = bufferLeft;
		posR = bufferRight;
		part = AudioPart::HIGH;
	}

	if( channels == AudioChannels::STEREO )
	{
		for( uint32_t i=0; i<AUDIO_BUFFER_SIZE; i++ )
		{
			AudioData *tmp = data+(i<<1);
			*(posL+i) = *(tmp);
			*(posR+i) = *(tmp+1);
		}
	}
	else
	{
		memcpy( posL, data, sizeof(AudioData)*AUDIO_BUFFER_SIZE );
		memcpy( posR, data, sizeof(AudioData)*AUDIO_BUFFER_SIZE );
	}
}

bool audioIsReady( void )
{
	if( part == AudioPart::LOW )
	{
		if( bufferIndex > AUDIO_BUFFER_SIZE )
			return true;
	}
	else if( bufferIndex < AUDIO_BUFFER_SIZE )
		return true;

	return false;
}

void audioStop( void )
{
	AUDIO_SAMPLING_TIMER->CR1 &= ~TIM_CR1_CEN;
	AUDIO_DAC_TIMER->CR1 &= ~TIM_CR1_CEN;
}

void audioStart( void )
{
	AUDIO_DAC_TIMER->CR1 |= TIM_CR1_CEN;
	AUDIO_SAMPLING_TIMER->CR1 |= TIM_CR1_CEN;
}
