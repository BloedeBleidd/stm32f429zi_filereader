/*
 * priorities.h
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef PRIORITIES_H_
#define PRIORITIES_H_


#define priorityIDLE		0U
#define priorityLOWEST		1U
#define priorityLOWER		2U
#define priorityLOW			3U
#define priorityNORMAL		4U
#define priorityHIGH		5U
#define priorityHIGHER		6U
#define priorityHIGHEST		7U
#define priorityREALTIME	8U


#endif /* PRIORITIES_H_ */
