/*
 * alloc.c
 *
 *  Created on: Sep 3, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "Trace.h"
#include "RTOS_includes.h"
#include "sdram.h"
#include <stdlib.h>

static uint32_t allocCnt = 0;
static uint32_t maxWaterMark = 0;

void * operator new( size_t size )
{
	vTaskSuspendAll();
    void * p = malloc( size );
    allocCnt++;
    uint32_t tmpWaterMark = (uint32_t)(p) + (uint32_t)(size);

    if( tmpWaterMark > maxWaterMark )
    {
    	maxWaterMark = tmpWaterMark;
    }

	trace_printf( "NEW addres=%X size=%d allocated=%d watermark=%X\r\n", p, size, allocCnt, maxWaterMark );
    xTaskResumeAll();
    return p;
}

void operator delete( void * p )
{
	vTaskSuspendAll();
	free(p);
	allocCnt--;
	trace_printf( "DELETE addres=%X allocated=%d watermark=%X\r\n", p, allocCnt, maxWaterMark );
	xTaskResumeAll();
}


